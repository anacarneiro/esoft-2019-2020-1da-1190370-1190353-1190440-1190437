@startuml
skinparam classAttributeIconSize 0

class PublicarTarefaUI {
}

class AplicacaoPOT {
  +static getInstance()
  +getSessaoAtual()
}

class SessaoUtilizador {
  +getEmailUtilizador()
}

class ListTarefa {
  +getTarefaPorReferencia(tarefaRef)
}

class Tarefa {
  -String referencia
  -String designacao
  -String descInformal
  -String descTecnica
  -Integer duracaoEst
  -Double custoEst
}

class ArrayList<Tarefa> {
}

class List <Tarefa> {
}

class Categoria {
  -String id
  -String descricao
}

class Regimento {
  -String designacao
  -String descricaoRegra
}

class Organizacao {
  -String nome
  -String NIF
  -String website
  -String telefone
  -String email
  +getListTarefas()
  +getRegistoColaborador()
}

class Anuncio {
  -Date dtInicioPublicacao
  -Date dtFimPublicacao
  -Date dtInicioAplicacao
  -Date dtFimAplicacao
  -Date dtInicioSeriacao
  -Date dtFimSeriacao
  +criar(dtInicioPublicacao, dtFimPublicacao, dtInicioAplicacao, dtFimAplicacao, dtInicioSeriacao, dtFimSeriacao, tarefa, reg)
}

class RegistoOrganizacao {
  -Organizacao orgReturn
  +getOrganizacaoPorEmailUtilizador(email)
  +getListTarefas(org)
}

class Plataforma {
  -String designacao
  +getRegistoOrganizacao()
  +getRegistoPublicarTarefa()
  +getTiposRegimentos()
  +getREgimentoPorDesignacao(email)
}

class RegistoPublicarTarefa {
+pubTarefa(dtInicioPublicacao, dtFimPublicacao, dtInicioAplicacao, dtFimAplicacao, dtInicioSeriacao, dtFimSeriacao, tarefa, reg)
+validaPubTarefa(pubTarefa)
+adicionarPubTarefa(pubTarefa)
+registopubTarefa(pubTarefa)
}

class PublicarTarefaController {
  +getListTarefas()
  +getTarefaPorReferencia(tarefaRef)
  +getTiposRegimentos()
  +novoPublicarTarefa(dtInicioPublicacao, dtFimPublicacao, dtInicioAplicacao, dtFimAplicacao, dtInicioSeriacao, dtFimSeriacao,tarefa, regDes)
  +registoPublicarTarefa
}

class RegistoColaborador {
+hasColaboradorComEmail(email)
}

class Colaborador {
  -String nome
  -String papel
  -String telefone
  -String email
  +temEmail(email)
}

PublicarTarefaUI --> PublicarTarefaController

PublicarTarefaController --> AplicacaoPOT
PublicarTarefaController --> SessaoUtilizador
PublicarTarefaController --> RegistoOrganizacao
PublicarTarefaController --> RegistoPublicarTarefa
PublicarTarefaController --> Plataforma

Plataforma "1" --> "*" Categoria : possui >
Plataforma "1" --> "1" RegistoOrganizacao : possui >
Plataforma "1" --> "1" RegistoPublicarTarefa : possui >
Plataforma "1" --> "1" Regimento : suporta >

RegistoOrganizacao "1" --> "*" Organizacao : possui >
Organizacao "1" --> "1" ListTarefa : possui >
ListTarefa "1" --> "1" ArrayList : é >
ArrayList "1" --> List : implementa >
Organizacao "1" --> "1" RegistoColaborador : possui >
RegistoColaborador "1" --> "*" Colaborador : possui >
ListTarefa "1" --> "*" Tarefa : possui >
Tarefa "*" --> "1" Categoria : encaixa em >
Tarefa "1" -- > "0..1" Anuncio : origina >
Tarefa "*" --> "1" Colaborador : especifica >

RegistoPublicarTarefa "1" --> "*" Anuncio : possui >
Anuncio "*" --> "1" Organizacao : refere-se a
Anuncio "*" --> "1" Colaborador : publicado por
Anuncio "*" --> "1" Regimento : guiado por >

@enduml
