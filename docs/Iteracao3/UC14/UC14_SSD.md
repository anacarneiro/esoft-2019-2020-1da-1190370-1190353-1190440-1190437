@startuml
hide footbox
autonumber

actor "Gestor da Organização" as GO
participant "Sistema" as SIST

activate GO

    GO -> SIST : inicia a adjudicação de um novo anúncio

    activate SIST
        SIST --> GO : apresenta a lista de todas as tarefas publicadas pela organização e pede que seja selecionado uma

    deactivate SIST

    GO -> SIST : seleciona uma tarefa publicada

    activate SIST
        SIST --> GO : apresenta os dados da melhor candidatura e solicita confirmação ao Gestor de Organização em relação à escolha daquele freelancer
    deactivate SIST

    GO -> SIST : confirma

    activate SIST
        SIST --> GO : solicita os dados necessários para adjudicar a tarefa publicada (i.e. período de realização da tarefa e remuneração)
    deactivate SIST

    GO -> SIST : introduz os dados solicitados

    activate SIST
        SIST --> GO : valida, apresenta os dados e solicita confirmação
    deactivate SIST

    GO -> SIST : confirma

    activate SIST
        SIST --> GO : atribui a tarefa e informa do sucesso da operação

deactivate GO

@enduml
