/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Bruna
 */
public class Candidatura {
    
    private double valor;
    private int dias;
    private String texto;

    public Candidatura() {
    }

    public Candidatura(double valor, int dias, String texto) {
        this.valor = valor;
        this.dias = dias;
        this.texto = texto;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    public boolean éCandidaturaElegivelParaEliminarDoFreelancer(Freelancer freel){
        return false;
    }
    
    public void setCandidatura(double valorPrt, int nrDias, String txtApres, String txtMotiv){
        
    }
    
}
