/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.String;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.pot.registo.RegistoAnuncio;
import pt.ipp.isep.dei.esoft.pot.registo.RegistoFreelancer;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Plataforma
{
    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<AreaAtividade> m_lstAreasAtividade;
    private final Set<CompetenciaTecnica> m_lstCompetencias;
    private final Set<Categoria> m_lstCategorias;
    private final Set<Freelancer> m_1stFreelancers;
    
    private String designacao;

    public Plataforma(String strDesignacao)
    {
        if ( (strDesignacao == null) ||
                (strDesignacao.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strDesignacao = strDesignacao;
      
        this.m_1stFreelancers = new HashSet<>();
        this.m_oAutorizacao = new AutorizacaoFacade();
        
        
        this.m_lstAreasAtividade = new HashSet<>();
        this.m_lstCompetencias = new HashSet<>();
        this.m_lstCategorias = new HashSet<>();
    }
    
    public AutorizacaoFacade getAutorizacaoFacade()
    {
        return this.m_oAutorizacao;
    }
    
    // Organizações
    
    // <editor-fold defaultstate="collapsed">  
    
    // Competências Tecnicas
    
    // <editor-fold defaultstate="collapsed">
    
    public CompetenciaTecnica getCompetenciaTecnicaById(String strId)
    {
        for(CompetenciaTecnica oCompTecnica : this.m_lstCompetencias)
        {
            if (oCompTecnica.hasId(strId))
            {
                return oCompTecnica;
            }
        }
        
        return null;
    }

    public CompetenciaTecnica novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoCompleta, AreaAtividade oArea)
    {
        return new CompetenciaTecnica(strId, strDescricaoBreve,strDescricaoCompleta,oArea);
    }

    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        if (this.validaCompetenciaTecnica(oCompTecnica))
        {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        return m_lstCompetencias.add(oCompTecnica);
    }
    
    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }
    // </editor-fold>
    
    // Areas de Atividade 
    // <editor-fold defaultstate="collapsed">
            
    public AreaAtividade getAreaAtividadeById(String strId)
    {
        for(AreaAtividade area : this.m_lstAreasAtividade)
        {
            if (area.hasId(strId))
            {
                return area;
            }
        }
        
        return null;
    }

    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        return new AreaAtividade(strCodigo, strDescricaoBreve, strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea)
    {
        if (this.validaAreaAtividade(oArea))
        {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea)
    {
        return m_lstAreasAtividade.add(oArea);
    }
    
    public boolean validaAreaAtividade(AreaAtividade oArea)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }
    
    public List<AreaAtividade> getAreasAtividade()
    {
        List<AreaAtividade> la = new ArrayList<>();
        la.addAll(this.m_lstAreasAtividade);
        return la;
    }
        public List<CompetenciaTecnica> getCompetencias()
    {
        List<CompetenciaTecnica> lc = new ArrayList<>();
            lc.addAll(this.m_lstCompetencias);
        return lc;
    }
            public List<Categoria> getCategorias()
    {
        List<Categoria> lt = new ArrayList<>();
            lt.addAll(this.m_lstCategorias);
        return lt;
    }
        
  
    
    // </editor-fold>

  public Categoria novaCategoria(String strDescricao, String strIdentificador, String strAreaAtividade, ArrayList<String> strCompetencias)
    {
        return new Categoria(strDescricao, strIdentificador, strAreaAtividade, strCompetencias);
    }

    public boolean registaCategoria(Categoria oCategoria)
    {
        if (this.validaCategoria(oCategoria))
        {
            return addCategoria(oCategoria);
        }
        return false;
    }

    private boolean addCategoria(Categoria oCategoria)
    {
        return m_lstCategorias.add(oCategoria);
    }
    
    public boolean validaCategoria(Categoria oCategoria)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }


    public CompetenciaTecnica novaCompetenciaTecnica(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getCategoriasByID(String cat) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Categoria getCategoriaById(String strId)
    {
        for(Categoria cat : this.m_lstCategorias)
        {
            if (cat.hasId(strId))
            {
                return cat;
            }
        }
        return null;


    }
    
    public RegistoFreelancer getRegistoFreelancer(){
        throw new UnsupportedOperationException("not implement yet");
    }
    
    public RegistoAnuncio getRegistoAnuncio(){
        throw new UnsupportedOperationException("not implement yet");
    }
    public RegistoFreelancer getRegistoFreelancers(){
        return null;
    }
    public RegistoOrganizacoes getRegistoOrganizacoes (){
        return null;
    }
    public RegistoAnuncio getRegistoAnuncios(){
        return null;
    }
   
}
    
    
