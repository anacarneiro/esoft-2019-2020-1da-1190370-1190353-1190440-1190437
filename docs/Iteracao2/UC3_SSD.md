@startuml
autonumber
actor "Administrativo" as ADM

participant ":Sistema" as SIST

activate ADM
ADM -> SIST : inicia definição de nova categoria de tarefa
activate SIST
SIST --> ADM : solicita a descrição
deactivate SIST

ADM -> SIST : introduz a descrição
activate SIST
SIST --> ADM : mostra a lista de áreas de atividades para que seja selecionada uma
deactivate SIST

ADM -> SIST : seleciona uma área de atividade
activate SIST
SIST --> ADM : mostra a lista de competências técnicas referentes à área de atividade previamente selecionada para que seja selecionada uma
deactivate SIST

loop
ADM --> SIST : escolhe uma competência técnica da lista
activate SIST

SIST --> ADM : solicita indicação do seu caráter (i.e. obrigatória ou desejável)
deactivate SIST

ADM --> SIST : introduz informação relativa ao caráter da competência técnica
activate SIST

SIST --> ADM : solicita o grau de proficiência
deactivate SIST

ADM --> SIST : introduz o valor pedido

loop end

activate SIST

SIST --> ADM : valida e apresenta os dados ao admnistrativo, pedindo confirmação dos mesmos

deactivate SIST

ADM --> SIST : confirma os dados

activate SIST

SIST --> ADM : regista os dados e informa ao admnistrativo do sucesso da operação

deactivate SIST
deactivate ADM

@enduml
