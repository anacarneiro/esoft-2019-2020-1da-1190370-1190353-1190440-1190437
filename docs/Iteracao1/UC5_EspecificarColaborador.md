# UC5 - Especificar Colaborador de Organização

## 1. Engenharia de Requisitos

### Formato Breve

O gestor é indicado aquando o registo da organização na plataforma.
O gestor de organização inicia o registo de novos colaboradores no sistema.
O sistema solicita os dados necessários sobre o colaborador (i.e. nome, função, contacto telefónico e email).
O gestor de organização introduz todos os dados solicitados. Em seguida o sistema valida e apresenta novamente os dados e pede confirmação ao gestor de organização.
Após a confirmação do gestor, o sistema regista os dados e informa o gestor do sucesso da operação.

### SSD
![UC5_SSD.svg](UC5_SSD.svg)


### Formato Completo

#### Ator principal

Gestor de Organização

#### Partes interessadas e seus interesses
* **Gestor de organização:** pretende registar novos colaboradores no sistema para que estes possam
ajudar na Organização.
* **Colaboradores(que não são gestores):** pretende conseguir interagir e aceder às informações.
* **T4J:** quer que a plataforma permita criar novos colaboradores das Organização, fazendo com que mais utilizadores usem a plataforma.


#### Pré-condições
A organização tem que estar registada na plataforma para que exista o organizador de organização,
pois este é o responsável pelo registo de novos colaboradores.


#### Pós-condições
O novo colaborador fica registado e especificado no sistema assim como a sua função na organização.

### Cenário de sucesso principal (ou fluxo básico)

1. O gestor de organização inicia a especificação de novos colaboradores.
2. O sistema solicita os dados necessários (i.e. nome, função, contacto telefónico e email).
3. O gestor de organização introduz os dados solicitados.
4. O sistema valida e apresenta os dados ao gestor de organização, pedindo que os confirme.
5. O gestor de organização confirma.
6. O sistema regista os dados e informa o gestor de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O gestor de organização solicita o cancelamento da especificação do colaborador.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o gestor de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o gestor de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O gestor de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O dado password é definido na criação do colaborador ou é gerado automático?
* O gestor de organização pode criar outro gestor de organização?
* O gestor de organização pode eliminar colaboradores?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC5_MD.svg](UC5_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O gestor de organização inicia a especificação de um novo colaborador.   		 |	... interage com o utilizador? | EspecificarColaboradorUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| EspecificarColaboradorController | Controller    |
|  		 |	... cria instância de Colaborador| Organização   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. nome, função, contacto telefónico e endereço de email)..  		 |							 |             |                              |
| 3. O gestor de organização introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Colaborador | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema valida e apresenta os dados ao gestor, pedindo que os confirme.   		 |	...valida os dados do Colaborador (validação local) | Colaborador                              |IE: Possui os seu próprios dados|
|	 |	...valida os dados do Colaborador(validação global) | Organização  | IE: A Organização tem Colaborador  |
| 5. O gestor de organização confirma.   		 |							 |             |                              |
| 6. O sistema regista os dados e informa o gestor do sucesso da operação.  		 |	... guarda o Colaborador criado? | Organização  | IE: No MD a organização tem Colaborador|


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organização
 * Colaborador


Outras classes de software (i.e. Pure Fabrication) identificadas:

 * EspecificarColaboradorUI
 * EspecificarColaboradorController


###	Diagrama de Sequência

![UC5_SD.svg](UC5_SD.svg)


###	Diagrama de Classes

![UC5_CD.svg](UC5_CD.svg)
