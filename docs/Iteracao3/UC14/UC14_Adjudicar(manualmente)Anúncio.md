# UC14 - Adjudicar/ Atribuir (manualmente) Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O Gestor de Organização inicia a adjudicação de um novo anúncio. O Sistema apresenta a lista de todas as tarefas publicadas pela organização e pede que seja selecionado uma.
O Gestor de Organização seleciona uma tarefa publicada. O Sistema apresenta os dados da melhor candidatura e solicita confirmação ao Gestor de Organização em relação à escolha daquele freelancer.
O Gestor de Organização confirma. O Sistema solicita os dados necessários para atdjudicar a tarefa publicada (i.e. período de realização da tarefa e remuneração).
O Gestor de Organização introduz os dados solicitados. O Sistema valida, apresenta os dados e solicita confirmação. O Gestor de Organização confirma. O Sistema atribui a tarefa e informa sobre o sucesso da operação.

### SSD
![UC14_SSD.svg](UC14_SSD.svg)

### Formato Completo

#### Ator Principal

* Gestor de Organização

#### Partes interessadas e seus interesses
* **Gestor de Organização:** pretende adjudicação anúncios.
* **Organização:** pretende que os seus colaboradores possam especificar tarefas para posterior publicação.
* **Freelancer:** pretende conhecer os anúncios a que pode candidatar-se.
* **T4J:** pretende a adjudicação de tarefas a freelancers.


#### Pré-condições
*

### Pós-condições
*

### Cenário de sucesso principal (ou fluxo básico)

1. O Gestor de Organização inicia a adjudicação de um novo anúncio.
2. O Sistema apresenta a lista de todas as tarefas publicadas pela organização e pede que seja selecionado uma.
3. O Gestor de Organização seleciona uma tarefa publicada.
4. O Sistema apresenta os dados da melhor candidatura e solicita confirmação ao Gestor de Organização em relação à escolha daquele freelancer.
5. O Gestor de Organização confirma.
6. O Sistema solicita os dados necessários para adjudicar a tarefa publicada (i.e. período de realização da tarefa e remuneração).
7. O Gestor de Organização introduz os dados solicitados.
8. O Sistema valida, apresenta os dados e solicita confirmação.
9. O Gestor de Organização confirma.
10. O Sistema atribui a tarefa e informa sobre o sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O Gestor de Organização solicita o cancelamento da adjudicação do anúncio.

> O caso de uso termina.

3a. O Gestor de Organização não seleciona uma tarefa publicada.
>       1. O Sistema pede seleção de uma tarefa publicada.
>       2. O Sistema permite uma nova seleção.
>
        > 2a. O Gestor de Organização não seleciona uma tarefa publicada. O caso de uso termina.

5a. O Gestor de Organização não confirma os dados.
>       1. O Sistema pede confirmação.
>       2. O Sistema permite nova confirmação.
>
        > 2a. O Gestor de Organização não confirma. O caso de uso termina.

7a. O Gestor de Organização não introduz os dados solicitados.
>       1. O Sistema pede introdução dos dados solicitados.
>       2. O Sistema permite a intrdução de novos dados.
>
        > 2a. O Gestor de Organização não altera os dados. O caso de uso termina.

8a. O Sistema deteta que os dados introduzidos são inválidos.
>       1. O Sistema alerta o Gestor de Organização para o facto.
>       2. O Sistema permite a sua alteração.
>
        > 2a. O Gestor de Organização não altera os dados. O caso de uso termina.

9a. O Gestor de Organização não confirma os dados.
>       1. O Sistema pede confirmação.
>       2. O Sistema permite nova confirmação.
>
        > 2a. O Gestor de Organização não confirma. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Os períodos são especificados em dias completos?
* É permitido que o mesmo freelancer efetue mais do que uma candidatura ao mesmo anúncio?
* Os Freelancers podem atualizar	a	informação	das candidaturas previamente	efetuadas por	si?
* Os Freelancers retirar	candidaturas	previamente	efetuadas por	si?
* O	processo	de	atribuição deve	ser	realizado pelo gestor	da	organização	sempre	que	a	atribuição	prevista	no	 regimento	é
opcional?
* O	processo	de	atribuição	deve	ser	realizado automaticamente	pelo	sistema,	quando	a	atribuição	está
especificada	pelo	regimento	como	sendo	obrigatória?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC14_MD.svg](UC14_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

TENHO ESTA MERDA TODA MAL, AMANHÃ COMPOR!!!

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Gestor de Organização inicia a adjudicação de um novo anúncio. |	... interage com o utilizador? | AdjudicarAnuncioUI |  Pure Fabrication |
| |	...coordena o UC? | AdjudicarAnuncioController | Controller |
| |	...conhece o utilizador a usar o sistema? | Sessaoutilizador | i.e: cf. documentação do componente do Gerenciamento dos Utilizadores |
| | ...conhece a Organização a que o Gestor pertence? | RegistoOrganizacao | i.e: no MD, Plataforma tem registado todas as Organizações e por execução de HC/LC delega em RegistoOrganizacao |
| | | Organizacao | i.e: sabe o seu Gestor |
| 2. O Sistema apresenta os dados da melhor candidatura e solicita confirmação ao Gestor de Organização em relação à escolha daquele freelancer. | ...conhece Anuncio?	| i.e: no MD, a plataforma contém Anuncio e por execução de HC/LC, delega em RegistoAnuncio | |
| | ...conhece RegistoAnuncio? | Plataforma | i.e: Plataforma contém RegistoAnuncio |
| | ...infere que o processo de Adjudicar/Atribuir seja opcional/obrigatório? | RegimentoTipo | i.e: no MD, ProcessoAdjudicacao segue as orientações do RegimentoTipo do Anúncio |
| | ...conhece o tipo de Regimento a ser seguido pelo Anuncio? | Anuncio | i.e: conhece os seus dados |
| 3. O Gestor de Organização seleciona uma tarefa publicada. | ...cria a instância ProcessoAdjudicacao? | RegistoAnuncio | Creator: no MD, ProcessoAdjudicacao refere-se a um processo único para Anuncio e por execução de HC/LC delega em RegistoAnuncio |
| | ...guarda a TarefaPublicada? | ProcessoAdjudicacao | i.e: conhece os seus dados |
| 4. O Sistema apresenta os dados da melhor candidatura e solicita confirmação ao Gestor de Organização em relação à escolha daquele freelancer. | ...conhece as Aplicacoes? | TarefaPublicada | i.e: no MD, TarefaPublicada recebe Aplicacoes |
| | ...conhece as Classificacoes da Aplicacao? | Classificacoes | i.e: no MD, a TarefaPublicada cria o ProcessoSeriacao, que resulta nas Aplicacoes receberem Classificacoes |
| | ...executa o ProcessoSeriacao | ProcessoSeriacao | i.e: conhece os seus dados |
| 5. O Gestor de Organização confirma. | ...guarda as informações das Aplicacoes (i.e. o Freelancer) para uso futuro? | ProcessoAdjudicacao | I.E: conhece os seus dados |
| 6. O Sistema solicita os dados necessários para adjudicar a tarefa publicada (i.e. período de realização da tarefa e remuneração). | | | |
| 7. O Gestor de Organização introduz os dados solicitados. | ...guarda os dados? | ProcessoAdjudicacao | I.E: conhece os seus dados |
| 8. O Sistema valida, apresenta os dados e solicita confirmação. | ...valida os dados do ProcessoAdjudicacao? | ProcessoAdjudicacao | I.E: possui os seus próprios dados |
| 9. O Gestor de Organização confirma. | | | |
| 10. O Sistema atribui a tarefa e informa sobre o sucesso da operação. | ...guarda o ProcessoAdjudicacao? | TarefaPublicada | I.E: processo único para a Tarefa Publicada |


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * Plataforma
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:

 * AdjudicarAnuncioUI
 * AdjudicarAnuncioController

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

###	Diagrama de Sequência

![UC14_SD.svg](UC14_SD.svg)

###	Diagrama de Classes

![UC14_CD.svg](UC14_CD.svg)
