/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.controller.AplicacaoPOT;

/**
 *
 * @author paulomaio
 */
public class EspecificarCompetenciaTecnicaUI
{
    private EspecificarCompetenciaTecnicaController m_controller;
    public EspecificarCompetenciaTecnicaUI()
    {
        m_controller = new EspecificarCompetenciaTecnicaController();
    }

    public void run()
    {
        System.out.println("\nEspecificar Competência Técnica:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCompetenciaTecnica()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados() {
        String strId = Utils.readLineFromConsole("Id: ");
        String strDescricaoBreve = Utils.readLineFromConsole("Descrição Breve: ");
        String strDescricaoDetalhada = Utils.readLineFromConsole("Descrição Detalhada: ");
        
        List<AreaAtividade> lc = m_controller.getAreasAtividade();
        
        String areaId = "";
        AreaAtividade area = (AreaAtividade)Utils.apresentaESeleciona(lc, "Selecione a Área de Atividaade a que é referente esta Competência Técnica:");
        if (area != null)
            areaId = area.getCodigo();
        
        return m_controller.novaCompetenciaTecnica(strId, strDescricaoBreve,strDescricaoDetalhada,areaId);
    }
    public class EspecificarCompetenciaTecnicaController
{
    private Plataforma m_oPlataforma;
    private CompetenciaTecnica m_oCompetenciaTecnica;
    public EspecificarCompetenciaTecnicaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }


    public boolean novaCompetenciaTecnica(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        try
        {
            this.m_oCompetenciaTecnica = this.m_oPlataforma.novaCompetenciaTecnica(strCodigo, strDescricaoBreve,strDescricaoDetalhada);
            return this.m_oPlataforma.validaCompetenciaTecnica(this.m_oCompetenciaTecnica);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCompetenciaTecnica = null;
            return false;
        }
    }


    public boolean registaCompetenciaTecnica()
    {
        return this.m_oPlataforma.registaCompetenciaTecnica(this.m_oCompetenciaTecnica);
    }

    public String getCompetenciaTecnicaString()
    {
        return this.m_oCompetenciaTecnica.toString();
    }

        private boolean novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, String areaId) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private List<AreaAtividade> getAreasAtividade() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nCompetência Técnica:\n" + m_controller.getCompetenciaTecnicaString());
    }
      
}