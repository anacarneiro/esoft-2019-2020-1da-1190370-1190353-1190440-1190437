/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

/**
 *
 * @author user
 */
public class Tarefa {

    private String refUnica;
    private String designacao;
    private String duracao;
    private String custo;
    private String descInfo;
    private String descTecnica;
    private String cat;

    public Tarefa(String refUnica, String designacao, String duracao, String custo, String descInfo, String descTecnica, String cat) {
        if ((refUnica == null) || (designacao == null) || (duracao == null) || (custo == null)
                || (descInfo == null) || (descTecnica == null) || (cat == null) || (refUnica.isEmpty()) || (designacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.refUnica = refUnica;
        this.designacao = designacao;
        this.duracao = duracao;
        this.custo = custo;
        this.descInfo = descInfo;
        this.descTecnica = descTecnica;
        this.cat = cat;

    }
    
    public Categoria getCategorTarefa(){
        throw new UnsupportedOperationException("not implement yet");
    }
    @Override
        public String toString()
    {
        return String.format("%s - %s - %s  - %s - %s - %s ", this.refUnica, this.descInfo, this.descTecnica, this.custo,this.duracao,this.designacao);
    }
    

}
