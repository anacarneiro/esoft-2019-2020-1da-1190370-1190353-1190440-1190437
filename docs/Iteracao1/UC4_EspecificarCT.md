# UC 4 - Especificar Competência Técnica

## 1. Engenharia de Requesitos

### Formato Breve

Depois da criação das áreas de atividade específicas, o administrador introduz os dados solicitados necessários para acabar uma área específica de atividade.
O sistema valida e mostra dos dados para o administrador, pedindo para os confirmar.

## SSD
![UC4_SSD.svg](UC4_SSD.svg)

## Formato Completo

### Ator Principal

Administrativos

#### Partes interessadas e seus interesses

* **Administrativo: ** pretende definir as áreas de atividade para que possa posteriormente catalogar as competências técnicas e categorias de tarefas.

* **T4J: ** pretende que a plataforma permita catalogar as competências técnicas e as categorias de tarefas em áreas de atividade.

### Pré-condições

Criação de áreas de atividade.


### Pós-condições

A informação da área de atividade é resgistada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo especifica a competência técnica.

2. O sistema solicita os dados necessários (i.e. código único, descrição breve e detalhada).

3. O administrativo introduz os dados solicitados.

4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.

5. O administrativo confirma.

6. O sistema regista os dados e informa o administrativo do sucesso da operação.




## Extensões (ou fluxos alternativos)


*a. O administrativo solicita o cancelamento da especificação da competência técnica.


> O caso de uso termina.


4a. Dados mínimos obrigatórios em falta.

> 1. O sistema informa quais os dados em falta.
> 2. O administrativo não altera os dados (passo 3).

>

    > 2a. O administrativo não altera os dados. O caso de uso termina.


4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

> 1. O sistema alerta o administrativo para o facto.
> 2. O sistema permite a sua alteração (passo 3)

>

     > 2a. O administrativo não altera os dados. O caso de uso termina.


4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

> 1. O sistema alerta o administrativo para o facto.
> 2. O sistema permite a sua alteração (passo 3).

>
     	> 2a. O administrativo não altera os dados. O caso de uso termina.



#### Requisitos especiais
\-


#### Lista de Variações de Tecnologias e Dados
\-


#### Frequência de Ocorrência
\-


#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC4_MD.svg](UC4_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo especifica a competência técnica necessária para a realização da tarefa uma área de atividade específica.   		 |	... interage com o utilizador? | EspecificarCompetenciaTecnicaUI    |  Pure Fabrication. |
|  		 |	... coordena o UC?	| EspecificarCompetenciaTecnicaController | Controller    |
|  		 |	... cria instâncias de CompetenciaTecnica? | Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários para a criação de uma competência técnica(código, descrição breve e detalhada).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   CompetenciaTecnica | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	...valida os dados da CompetenciaTecnica (validação local) | CompetenciaTecnica |                              |IE. Possui os seu próprios dados.|  	
|	 |	...valida os dados da CompetenciaTecnica (validação global) | Plataforma  | IE: A Plataforma possui/agrega CompetenciaTecnica  |
| 5. O administrativo confirma.   		 |							 |             |                              |
| 6. O sistema mostra como concluída a especificação da competência técnica e informa o administrativo do sucesso da operação.  		 |	... guarda a CompetenciaTecnica criada? | Plataforma  | IE: No MD a Plataforma possui CompetenciaTecnica|  

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CompetenciaTecnica


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarCompetenciaTecnicaUI  
 * EspecificarCompetenciaTecnicaController


###	Diagrama de Sequência

![UC4_SD.svg](UC4_SD.svg)


###	Diagrama de Classes

![UC4_CD.svg](UC4_CD.svg)
