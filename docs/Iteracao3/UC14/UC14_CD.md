@startuml
skinparam classAttributeIconSize 0

class AdjudicarAnuncioUI {
}

class AdjudicarAnuncioController {
+getPubTarefaAdjudicacaoOpcional()
+getPrimeiraAplicacao(IdPubTarefa)
+getFreelancer()
+novoProcessoAdjudi(org,pubTarefa,primeiraAplicacao)
+registoProcessoAdjudi()
}

class AplicacaoPOT {
  +getInstancia()
  +getSessaoAtual()
}

class SessaoUtilizador {
  +getEmailUtilizador()
}

class Plataforma {
  -String designacao
  +getRegistoOrganizacao()
  +getRegistoPublicarTarefa()
}

class RegistoOrganizacao {
  +getOrganizacaoPorEmailUtilizador(email)
}

class RegistoPublicarTarefa {
+getPubTarefaAdjudicacaoOpcional(org)
+getPubTarefa(pubTarefa)
+getQtdDeProcessosDeAdjudi(anoAtual)
}

class Organizacao {
  -String nome
  -String NIF
  -String website
  -String telefone
  -String email
  +temColaboradorComEmail(email)
}

class TarefaPublicada {
  -Date dtInicioPublicacao
  -Date dtFimPublicacao
  -Date dtInicioAplicacao
  -Date dtFimAplicacao
  -Date dtInicioSeriacao
  -Date dtFimSeriacao
  -Date dtInicioDecisao
  -Date dtFimDecisao
  +getListaAplicacao()
  +getTarefa()
  +generarSeqNum(processoAdjudi)
  +setProcessoAdjudi(ProcessoAjudi)
  +novoProcessoAdjudi(org,freelancer,pubTarefa,tarefa,numDias,valorRemun,numAdjudi)
}

class Colaborador {
  -String nome
  -String papel
  -String telefone
  -String email
  +temEmail(email)
}

class Tarefa {
  -String referencia
  -String designacao
  -String descInformal
  -String descTecnica
  -int duracaoEst
  -Double custoEst
}

class ListaAplicacao {
  +getAplicacoes()
  +getPrimeiraAplicacao()
}

class ProcessoAdjudicacao {
-String SeqNumero
-Date dtAdjudicacao
-tarefaIniciarRealizacao
-tarefaFimRealizacao
-Double valorRemuneracao
+ProcessoAdjudicacao(adjuSeqNum,org,freelancer,tarefaPub,tarefa,iniciarTarefa,acabarTarefa,valorRemun,tempoAdjudi)
}

class Freelancer {
-String nome
-String NIF
-String telefone
-String email
+atribuir(ProcessoAdjudi)
}

class TipoRegimento {
-String designacao
-String descricaoRegra
}

class ListaAplicacao {
+getAplicacoes()
+getPrimeiraAplicacao()
}

class Aplicacao {
-Date dtAplicacao
-Double valorEsperado
-int numDias
-String textoApresent
-String textoMotiv
+getFreelancer()
+getValorRemun()
+getNumDias()
}



  //   PublicarTarefaController --> Plataforma
  //   Plataforma "1" --> "*" Categoria : possui >

AdjudicarAnuncioUI --> AdjudicarAnuncioController

AdjudicarAnuncioController --> AplicacaoPOT
AdjudicarAnuncioController --> Plataforma
AdjudicarAnuncioController --> SessaoUtilizador

Plataforma "1" --> "1" RegistoOrganizacao : tem >
Plataforma "1" --> "*" TipoRegimento : suporta >
Plataforma "1" --> "1" RegistoPublicarTarefa : tem >

RegistoOrganizacao "1" --> "*" Organizacao : tem resgistado >

Organizacao "1" --> "1..*" Colaborador : tem >
Organizacao "1" --> "1" Colaborador : tem gestor >
Organizacao "1" --> Tarefa : contém >

Colaborador "1" --> Tarefa : cria >

RegistoPublicarTarefa "1" --> "*" TarefaPublicada : publica >

TarefaPublicada "0..1" --> Tarefa : publica >
TarefaPublicada "1" --> "0..1" ProcessoAdjudicacao : origina >
TarefaPublicada "*" --> "1" TipoRegimento : guiado por >
TarefaPublicada "1" --> "1" ListaAplicacao : recebe >

ListaAplicacao "1" --> "*" Aplicacao : contém >

Aplicacao "*" --> "1" Freelancer : feito por >

@enduml
