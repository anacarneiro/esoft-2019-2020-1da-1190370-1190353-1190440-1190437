@startuml
hide methods
left to right direction
skinparam classAttributeIconSize 0


class Plataforma {
  -String designacao
}

class Organizacao {
  -String nome
  -String NIF
  -String website
  -String telefone
  -String email
}

class Tarefa {
  -String referencia
  -String designcao
  -String descInformal
  -String descTecnica
  -Integer duracaoEst
  -Double custoEst
}

class Categoria {
  -String id
  -String descricao
}

class Colaborador {
  -String nome
  -String funcao
  -String telefone
  -String email
}

class Utilizador {
  -String nome
  -String email
  -String password
}

class Anuncio{
  -Date InicioPublicitacao
  -Date FimPublicitacao
  -Date InicioApresentacao
  -Date FimApresentacao
  -Date InicioSeriacao
  -Date FimSeriacao
}

class Regimento {
  -String designacao
  -String descricaoRegra
}

Plataforma "1" -- "*" Organizacao : tem registadas >
Plataforma "1" -- "*" Categoria : possui >
Plataforma "1" -- "*" Anuncio  : possui >
Plataforma "1" -- "*" Regimento : suporta >
Organizacao "1" -- "*" Tarefa : possui >
Organizacao "1" -- "1..*" Colaborador : tem >
Organizacao "1" -- "*" Anuncio : refere-se a <
Colaborador "0..1" -- "1" Utilizador : atua como >
Colaborador "0..1" -- "*" Anuncio : publicado por >
Tarefa "1" -- "0..1" Anuncio : origina
Tarefa "*" -- "1" Categoria : enquadra-se em >
Tarefa "*" -- "1" Colaborador : especificada por >
Anuncio "*" -- "1" Regimento : guiado por >


@enduml
