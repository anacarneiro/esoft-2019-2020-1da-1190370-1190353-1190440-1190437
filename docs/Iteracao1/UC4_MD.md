@startuml
hide methods
left to right direction

class Plataforma {
  -String designacao
}

class AreaAtividade {
  -String codigo
  -String descBreve
  -String descDetalhada

}

class CompetenciaTecnica {
  -String codigo
  -String descBreve
  -String descDetalhada
}

class Administrativo {

}

class Utilizador {
  -String nome
  -String email
  -String password
}

Plataforma "1" -- "*" AreaAtividade : possui >
Plataforma "1" -- "*" CompetenciaTecnica : possui >
Plataforma "1" -- "*" Administrativo : tem >
Administrativo "0..1" -- "1" Utilizador: atua como >
Administrativo "1.." -- "1..*" AreaAtividade: especifica >
Administrativo "1..*" -- "1..*" CompetenciaTecnica: referente a >
CompetenciaTecnica "*" -- "1" AreaAtividade : referente a >

@enduml
