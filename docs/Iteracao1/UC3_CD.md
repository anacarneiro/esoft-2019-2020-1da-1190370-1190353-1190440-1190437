@startuml
class DefinirCategoriaUI {

}
hide DefinirCategoriaUI methods
hide DefinirCategoriaUI fields
class DefinirCategoriaController {
novaCategoria(String Descricao, String Identificador, String AreasAtividade, ArrayList<String> Competencias)
registarCategoria()
getAreasdeAtividade()
getCompetenciasTecnicas()
Obrigatoriedade()
}
class Categoria {
-String Identificador;
-String Descricao;
-String AreaAtividade;
-String[] Competencias;
Categoria(String Descricao, String Identificador, String AreaAtividade, ArrayList<String> Competencias)
}
hide DefinirCategoriaController fields
DefinirCategoriaUI ..> DefinirCategoriaController
DefinirCategoriaController ..> Plataforma
DefinirCategoriaController ..> Categoria
DefinirCategoriaController ..> Obrigatoria
class Obrigatoria {
Obrigatoriedade()
}
Plataforma -- Categoria : possui
class Plataforma {
-String designacao
Categoria novaCategoria(String Descricao, String Identificador, String AreaAtividade, ArrayList<String> Competencias)
validaCategoria(Categoria cat)
registaCategoria(Categoria cat)
addCategoria(Categoria cat)
}
hide Obrigatoria fields
@enduml