@startuml
autonumber
'hide footbox
actor "Colaborador de Organização" as ADM

participant ":PublicarTarefaUI" as UI
participant ":PublicarTarefaController" as CTRL
participant "AplicacaoPOT" as AP
participant "app\n:AplicacaoPOT" as APP
participant "sessao\n:SessaoUtilizador" as SESSAO
participant ":Plataforma" as PF
participant "ro\n:RegistoOrganizacao" as RO
participant ":List<Organizacao>" as LIST_O
participant "org\n:Organizacao" as ORG
participant "rc\n:RegistoColaborador" as RC
participant ":List<Colaborador>" as LIST_C
participant "colab\n:Colaborador" as COLAB
participant "rpt\n:RegistoPublicarTarefa" as RPT
participant "anuncio\n:Anuncio" as ANUNCIO
participant "lt\n:List<Tarefa>" as LIST_T
participant "lr\n:List<Regimento>" as LIST_R



activate ADM
ADM -> UI : inicia a publicação de uma tarefa
activate UI
UI --> CTRL : lt=getListTarefas()
activate CTRL

CTRL --> AP : app=getInstancia()

activate AP
deactivate AP

CTRL --> APP : sessao=getSessaoAtual()

activate APP
deactivate APP

CTRL --> SESSAO : email=getEmailUtilizador()

activate SESSAO
deactivate SESSAO

CTRL --> PF : ro=getRegistoOrganizacao()

activate PF
deactivate PF

CTRL --> RO : org=getOrganizacaoPorEmailUtilizador(email)

activate RO

RO --> RO : orgReturn = null

loop

RO --> LIST_O : org=get(i)

activate LIST_O
deactivate LIST_O

RO --> ORG : rc=getRegistoColaborador()

activate ORG
deactivate ORG

RO --> RC : temColaboradorComEmail(email)

activate RC

deactivate RO

RC --> RC : found=false

    loop

RC --> LIST_C : colab=get(i)

activate LIST_C
deactivate LIST_C

RC --> COLAB : encontrado=temEmail()

activate COLAB
deactivate COLAB

deactivate RC

    loop end

activate RO

RO --> RO : orgReturn=org

deactivate RO

loop end

CTRL --> RO : lt=getListTarefas(org)

activate RO

RO --> ORG : getListTarefas()

activate ORG
deactivate ORG

deactivate RO

deactivate CTRL

UI --> ADM : apresenta a lista de tarefas para publicar

deactivate UI

ADM --> UI : seleciona uma tarefa
activate UI

UI --> ORG : tarefa=getTarefaPorReferencia(tarefaRef)

activate ORG
deactivate ORG

UI --> PF : getTiposRegimento()

activate PF
deactivate PF

UI --> ADM : Pede os dados para a publicação (i.e. periodos e tipos de regimentos (através da lista))

deactivate UI

ADM --> UI : Introduz os dados pedidos

activate UI

UI --> CTRL : novaPubTarefa(dtInicioPublicacao, dtFimPublicacao, dtInicioAplicacao, dtInicioAplicacao, dtInicioSeriacao, dtFimSeriacao, tarefa, regDes)

activate CTRL

CTRL --> PF : reg=getRegimentoPorDesignacao(regDesc)

activate PF

CTRL --> PF : rpt=getRegistoPublicarTarefa()

deactivate PF

CTRL --> RPT : pubTarefa=novaPubTarefa(dtInicioPublicacao, dtFimPublicacao, dtInicioAplicacao, dtFimAplicacao, dtInicioSeriacao, dtFimSeriacao, tarefa, reg)

deactivate CTRL

activate RPT

RPT --> ANUNCIO : criar(dtInicioPublicacao, dtFimPublicacao, dtInicioAplicacao, dtInicioAplicacao, dtInicioSeriacao, dtFimSeriacao, tarefa, reg)

activate ANUNCIO
deactivate ANUNCIO

RPT --> RPT : validaPubTarefa(pubTarefa)

deactivate RPT

UI --> ADM : valida os dados e pede confirmação dos mesmo

deactivate UI

ADM --> UI : confirma os dados

activate UI

UI --> CTRL : registaPubTarefa()

activate CTRL

CTRL --> RPT : registaPubTarefa(pubTarefa)

activate RPT

RPT --> RPT: validaPubTarefa(pubTarefa)
RPT --> RPT: addPubTarefa(puTarefa)

UI --> ADM : informa do sucesso da operação

deactivate RPT
deactivate CTRL
deactivate UI

deactivate ADM

@enduml
