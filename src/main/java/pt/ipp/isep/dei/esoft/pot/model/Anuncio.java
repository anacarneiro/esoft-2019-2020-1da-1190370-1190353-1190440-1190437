/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.registo.RegistoCandidatura;

/**
 *
 * @author Bruna
 */
public class Anuncio {
    
    private Date inicioPublicacao;
    private Date fimPublicacao;
    private Date inicioApresentacao;
    private Date fimApresentacao;
    private Date inicioSeriacao;
    private Date fimSeriacao;
    private String tipoSeriacao;
    private Tarefa tarefa;
    private RegistoCandidatura registoCandidatura;

    public Anuncio() {
    }

    
    public Anuncio(Date inicioPublicacao, Date fimPublicacao, Date inicioApresentacao, Date fimApresentacao, Date inicioSeriacao, Date fimSeriacao, String tipoSeriacao) {
        this.inicioPublicacao = inicioPublicacao;
        this.fimPublicacao = fimPublicacao;
        this.inicioApresentacao = inicioApresentacao;
        this.fimApresentacao = fimApresentacao;
        this.inicioSeriacao = inicioSeriacao;
        this.fimSeriacao = fimSeriacao;
        this.tipoSeriacao = tipoSeriacao;
    }

    public Date getInicioPublicacao() {
        return inicioPublicacao;
    }

    public void setInicioPublicacao(Date inicioPublicacao) {
        this.inicioPublicacao = inicioPublicacao;
    }

    public Date getFimPublicacao() {
        return fimPublicacao;
    }

    public void setFimPublicacao(Date fimPublicacao) {
        this.fimPublicacao = fimPublicacao;
    }

    public Date getInicioApresentacao() {
        return inicioApresentacao;
    }

    public void setInicioApresentacao(Date inicioApresentacao) {
        this.inicioApresentacao = inicioApresentacao;
    }

    public Date getFimApresentacao() {
        return fimApresentacao;
    }

    public void setFimApresentacao(Date fimApresentacao) {
        this.fimApresentacao = fimApresentacao;
    }

    public Date getInicioSeriacao() {
        return inicioSeriacao;
    }

    public void setInicioSeriacao(Date inicioSeriacao) {
        this.inicioSeriacao = inicioSeriacao;
    }

    public Date getFimSeriacao() {
        return fimSeriacao;
    }

    public void setFimSeriacao(Date fimSeriacao) {
        this.fimSeriacao = fimSeriacao;
    }

    public String getTipoSeriacao() {
        return tipoSeriacao;
    }

    public void setTipoSeriacao(String tipoSeriacao) {
        this.tipoSeriacao = tipoSeriacao;
    }

    public Tarefa getTarefa() {
        throw new UnsupportedOperationException("not implement yet");
    }

    public RegistoCandidatura getRegistoCandidatura() {
        throw new UnsupportedOperationException("not implement yet");
    }
    public ListaCandidatura getListaCandidaturas(){
        return null;
    }
    

public void novoProcessoSeriacao(Colaborador colab){
    
}
public void registaProcessoSeriacao(ps){
    
}
private boolean validaProcessoSeriacao(Seriacao ps){
    
}
private void setProcessoSeriacao(Seriacao ps){
    
}
public void atribuirCandidaturaSubmetida(){
    
}
public void novoProcessoAtribuicao(Regimento tiposReg){
    
}

public String getId(){
    return null;
}
public boolean registaProcessoAtribuicao(ProjetoAtribuicao pa){
    return false;
}
public boolean validaProcessoAtribuicao(ProjetoAtribuicao pa){
    return false;
}
public void setProcessoAtribuicao(ProjetoAtribuicao pa){
    
}
    public Candidatura getCanByID(String canID){
        return null;
    }
}
