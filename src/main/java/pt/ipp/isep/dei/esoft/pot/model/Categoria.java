/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author 35193
 */
public class Categoria {

  
    private String m_strIdentificador;
    private String m_strDescricao;
    private String m_strAreaAtividade;
    private ArrayList<String> m_strCompetencias;

            
    
    public Categoria(String strDescricao, String strIdentificador, String strAreaAtividade, ArrayList<String> strCompetencias)
    {
        if ((strDescricao == null)||(strDescricao.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strDescricao = strDescricao;
        this.m_strIdentificador= strIdentificador;
        this.m_strAreaAtividade = strAreaAtividade;
        this.m_strCompetencias=strCompetencias; 
    }

  
    
    public boolean hasId(String m_strIdentificador)
    {
        return this.m_strIdentificador.equalsIgnoreCase(m_strIdentificador);
    }
    
    public String getIdentificador()
    {
        return this.m_strIdentificador;
    }
   
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.m_strIdentificador);
        return hash;
    }
    
    @Override
    public boolean equals(Object o) {
        // Inspirado em https://www.sitepoint.com/implement-javas-equals-method-correctly/
        
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        // field comparison
        Categoria obj = (Categoria) o;
        return (Objects.equals(m_strIdentificador, obj.m_strIdentificador));
    }
    
    public CompetenciaTecnica getCompetenciaTecnica(){
        throw new UnsupportedOperationException("not implement yet");
    }

    @Override
    public String toString()
    {
        return String.format("%s - %s - %s ", this.m_strIdentificador, this.m_strDescricao, this.m_strCompetencias);
    }

}


