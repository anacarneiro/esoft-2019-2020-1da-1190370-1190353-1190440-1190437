@startuml
Actor Freelancer
Participant ":Sistema"
activate Freelancer
Freelancer -> ":Sistema" : inicia a atualização da candidatura
activate ":Sistema"
":Sistema" --> Freelancer : apresenta lista e solicita a candidatura a ser atualizada
Freelancer -> ":Sistema" : seleciona candidatura
":Sistema" --> Freelancer : solicita dados atualizáveis (valor pretendido, número de dias, texto motivação/apresentação opcional)
Freelancer -> ":Sistema" : introduz dados que deseja atualizar
":Sistema" --> Freelancer : apresenta os dados e solicita confirmação
Freelancer -> ":Sistema" : confirma os dados
deactivate Freelancer
deactivate ":Sistema"
@enduml