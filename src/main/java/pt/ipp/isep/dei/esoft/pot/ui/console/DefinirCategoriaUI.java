/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.DefinirCategoriaController;
import pt.ipp.isep.dei.esoft.pot.controller.EspecificarAreaAtividadeController;
import pt.ipp.isep.dei.esoft.pot.controller.EspecificarCompetenciaTecnicaController;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author 35193
 */
public class DefinirCategoriaUI

 {
    private DefinirCategoriaController m_controller;
    public DefinirCategoriaUI()
    {
        m_controller = new DefinirCategoriaController();
    }

    public void run()
    {
        System.out.println("\nDefinir Categoria:");
        
        List<AreaAtividade> la = m_controller.getAreasAtividade();
        List<CompetenciaTecnica> l = m_controller.getCompetencias();
        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCategoria()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados() {
        String strDescricao = Utils.readLineFromConsole("Descricao: ");
        String strIdentificador = Utils.readLineFromConsole("Identificador: ");
        String AreaAtividade = Utils.readLineFromConsole("Area de Atividade: ");
        ArrayList<String> strCompetencias= Utils.readLineFromConsole("Competencias: ");
                
        return m_controller.novaCategoria(strDescricao, strIdentificador, AreaAtividade, strCompetencias);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nCategoria:\n" + m_controller.getCategoriaString());
    }
}