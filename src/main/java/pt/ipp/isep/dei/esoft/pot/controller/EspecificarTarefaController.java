
package pt.ipp.isep.dei.esoft.pot.controller;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Categoria;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author user
 */
public class EspecificarTarefaController {
   
   
        private Plataforma m_oPlataforma;
        private Organizacao m_oOrganizacao;
        private Tarefa m_oTarefa;
        private Categoria m_oCategoria;
   
    public EspecificarTarefaController(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador Não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
       public List<Categoria> getCategoriaTarefa()
    {
        return this.m_oPlataforma.getCategorias();
    }
        public boolean novaTarefa(String refUnica, String designacao, String duracao, String custo, String descInfo, String descTecnica,Categoria catID)
    {
        try
        {
            String cat = this.m_oPlataforma.getCategoriasByID(cat);
            this.m_oTarefa = this.m_oOrganizacao.novaTarefa(refUnica,  designacao,  duracao,  custo, descInfo, descTecnica, cat);
            return this.m_oOrganizacao.validaTarefa(this.m_oTarefa);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oTarefa= null;
            return false;
        }
    }
           
   
   
    public boolean registaTarefa()
    {
        return this.m_oOrganizacao.registaTarefa();
    }
    public String getTarefaString()
    {
        return this.m_oTarefa.toString();
    }
}
