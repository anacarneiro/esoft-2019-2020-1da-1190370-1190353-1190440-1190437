@startuml
skinparam classAttributeIconSize 0


left to right direction

class AtualizarCandidaturaUI {

}
class AtualizarCandidaturaController {
+getCandidaturas()
+getCanByID(String anuID,String canID)
+setCandidatura(double valorPrt, int  nrDias, String txtApres, String txtMotiv)
+AtualizaCand()
}
class Plataforma {
  -String designacao
+getRegistoFreelancers()
+getRegistoAnuncios()
}
class RegistoAnuncios {
+getCandidaturas(Freelancer freel)
+verificarDataLimite(Date dtFimCandidatura, Date dataAtual)
+getAnuncioByID(String anuID)
+AtualizaCand(Anuncio anu, Candidatura can)
+registaCandidaturaAtualizada()
}
class RegistoFreelancers {
+getFreelancerByEmail(Email email)
}

class Tarefa {
  -String referencia
  -String designacao
  -String descInformal
  -String descTecnica
  -Integer duracaoEst
  -Double custoEst
}

class Freelancer {
    -String nome
    -String NIF
    -String telefone
    -String email
}


class Anuncio
{
    -Date dtInicioPublicitacao
    -Date dtFimPublicitacao
    -Date dtInicioCandidatura
    -Date dtFimCandidatura
    -Date dtInicioSeriacao
    -Date dtFimSeriacao
+getCanByID(String canID)
}

class Candidatura
{
    -Date dataCandidatura
    -Double valorPretendido
    -Integer nrDias
    -String txtApresentacao
    -String txtMotivacao
+setCandidatura(double valorPrt, int  nrDias, String txtApres, String txtMotiv)
}


AtualizarCandidaturaUI .. AtualizarCandidaturaController
AtualizarCandidaturaController .. Plataforma
Plataforma "1" -- "*" RegistoAnuncios : possui >
Plataforma "1" --  "*" RegistoFreelancers : possui >
RegistoAnuncios  --  Anuncio : agrega >
RegistoFreelancers   --  Freelancer : agrega >




Anuncio "0..1" -- "1" Tarefa: publicita >
Anuncio "0..1" -- "1" Tarefa: da origem <
Anuncio "1" -- "*" Candidatura: recebe >



Candidatura "*" -- "1"  Freelancer: realizada por >
Candidatura "*" -- "1"  Freelancer: atualizada por >
@enduml