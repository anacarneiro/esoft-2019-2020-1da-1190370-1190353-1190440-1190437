@startuml
skinparam classAttributeIconSize 0



class EnderecoPostal {
  -String local
  -String codPostal
  -String localidade
  +EnderecoPostal(local,codPostal,localidade)
}

class Freelancer {
-String nome
-String NIF
-String contacto telefónico
-String email
  +Freelancer(nome, NIF, endereço postal, contacto telefónico, email)
  +{static} EnderecoPostal novoEndereco(local,codPostal,localidade)
}
class CompetenciaTecnica {
-String nome
-String descBreve
-String descDetalhada
}
class Plataforma {
  -String designacao
  
  +Freelancer novoFreelancer(nome,NIF,endereco postal, contacto telefonico, email)
  +validaFreelancer(Freelancer fre, pwd)
  -addFreelancer(Freelancer fre)
}


class RegistarFreelancerController {
    +novoFreelancer(nome,NIF,endereco postal, contacto telefonico, email, pwd)
    +registaFreelancer()
    +addFormacao(freelancer, instituicao, designacao do curso, grau, media de curso)
    +addCT(freelancer, competencia tecnica, grau)
}

class RegistarFreelancerUI {
}

RegistarFreelancerUI ..> RegistarFreelancerController
RegistarFreelancerController ..> Plataforma
RegistarFreelancerController ..> Freelancer
Freelancer --> CompetenciaTecnica : possui >
Freelancer --> Formacao : possui >
class Formacao {
-String designação do curso
-String instituição 
-int grau
-int média de curso
}
RegistarFreelancerController ..> AlgoritmoGeradordePassword
Plataforma --> AlgoritmoGeradordePassword : recorre a
interface AlgoritmoGeradordePassword {
  +geraPwd(nome, email)
  }
(Freelancer, CompetenciaTecnica).GraudeProficiencia
Class GraudeProficiencia {
-int grau de proficiencia
}
Plataforma ..> EnderecoPostal
Plataforma "1" --> "*" Freelancer : tem registados
Freelancer "1" --> "1" EnderecoPostal: localizado em
@enduml