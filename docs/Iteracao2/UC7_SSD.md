@startuml
Participant Administrativo
Participant ":Sistema"
Administrativo -> ":Sistema" : inicia registo
":Sistema" -> Administrativo : requisita dados (nome, NIF, endereçoc postal, contacto telefónico, email, competencias tecnicas, grau, designação do curso, instituição que concedeu o grau, média de curso, reconhecimentos)
Administrativo -> ":Sistema" : introduz as informações solicitadas pelo sistema 
":Sistema" -> Administrativo : registo completo com sucesso
@enduml