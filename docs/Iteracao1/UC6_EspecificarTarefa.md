# UC6 - Especificar tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a especificação de uma determinada tarefa. O sistema solicita os dados necessários
(i.e. referência	 única	 por	 organização, designação, descrição	informal	e	outra	de	carácter	técnico, 	estimativa
de	 duração, custo	 e	 a	 categoria	 em	 que	 a	 mesma	 se	 enquadra).
O colaborador de organização introduz os dados solicitados. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
O colaborador de organização confirma. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.

### SSD
![UC6_SSD](UC6_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de organização** pretende especificar uma determinada tarefa e inseri-la numa área de atividade na
plataforma de forma a usufruir da mesma.
* **T4J:** pretende que a plataforma permita especificar uma tarefa definida dentro de uma organização e integrada numa categoria de tarefa previamente criada.
* **Organização:** pretende publicar uma tarefa


#### Pré-condições
A organização tem que estar previamente registada na plataforma e o gestor de organização deve ter especificado o colaborador de organização como
utilizador e identificar o seu papel (nome, função, contacto telefónico, endereço de email).

#### Pós-condições
A tarefa fica registada e especificada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização  inicia a especificação de uma determinada tarefa.
2. O sistema solicita os dados (designação, descricão de caracter tecnico e informal, duração e custo) e solicita uma categoria de tarefa.
3. O colaborador de organização introduz os dados solicitados e integra a tarefa numa categoria.
4. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
5. O colaborador de organização confirma.
6. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da especificação da tarefa.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador de organização não altera os dados. O caso de uso termina.
4d. O colaborador de organização não foi especificado pelo gestor de organização.
> 1. O sistema informa o colaborador de organização para o facto.
> 2. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O identificador único é introduzido pelo colaborador de organização ou é automaticamente gerado pelo sistema?
* Qual a frequência de ocorrência deste caso de uso?
* O colaborador de uma organização não se pode registar como tal?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.svg](UC6_MD.svg)
## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização  inicia a especificação de uma determinada tarefa.   		 |	... interage com o utilizador? | EspecificarTarefaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| EspecificarTarefaController | Controller    |
|  		 |	... cria instância de Tarefa| Organização   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. O sistema solicita os dados necessários
(i.e. identificador único, designação, descrição informal e técnica, duração, custo).  		 |							 |             |                              |
| 3. O colaborador de organização introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Tarefa | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.   		 |	...valida os dados da tarefa (validação local) | Tarefa |                              |IE. Possui os seu próprios dados.|
|	 |	...valida os dados da Tarefa (validação global) | Organização  | IE: A Organização possui/publica Tarefa  |
| 5. O colaborador de organização confirma.   		 |							 |             |                              |
| 6. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.  		 |	... guarda a Tarefa criada? | Organização  | IE: No MD a Organizacao e responsavel por publicar a Tarefa|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organização
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:

 * EspecificarTarefaUI
 * EspecificarTarefaController


###	Diagrama de Sequência

![UC6_SD](UC6_SD.svg)


###	Diagrama de Classes

![UC6_CD.svg](UC6_CD.svg)
