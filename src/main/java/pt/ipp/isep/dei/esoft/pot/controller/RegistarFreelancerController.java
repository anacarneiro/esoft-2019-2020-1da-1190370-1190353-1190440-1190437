/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.EnderecoPostal;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author 35193
 */
public class RegistarFreelancerController
{
    private AplicacaoPOT m_oApp;
    private Plataforma m_oPlataforma;
    private Freelancer m_oFreelancer;
    private String m_strPwd;
    private Organizacao m_oOrganizacao;
    public RegistarFreelancerController()
    {
        this.m_oApp = AplicacaoPOT.getInstance();
        this.m_oPlataforma = m_oApp.getPlataforma();
    }
    
    
    public boolean novoFreelancer(String strNome, String strNIF, String strTelefone, 
            String strEmail, String strLocal, String strCodPostal, String strLocalidade)
    {
        try
        {
            String strPwd = null;
            this.m_strPwd = strPwd;
            EnderecoPostal oMorada = Organizacao.novoEnderecoPostal(strLocal, strCodPostal, strLocalidade);   
            this.m_oFreelancer = this.m_oPlataforma.novoFreelancer(strNome, strNIF, strTelefone, strEmail, oMorada);
            return this.m_oPlataforma.validaFreelancer(this.m_oFreelancer, this.m_strPwd);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oOrganizacao = null;
            return false;
        }
    }
    
    
    public boolean registaFreelancer()
    {
        return this.m_oPlataforma.registaFreelancer(this.m_oFreelancer, this.m_strPwd);
    }

    public String getFreelancerString()
    {
        return null;

    }
 //   public void addFormacao(Freelancer fre, String instituicao, String designacaodocurso, int media de curso)
    {
        
    }
    public void addCT(Freelancer fre, CompetenciaTecnica ct, GraudeProficiencia grau){
        
    }
}
