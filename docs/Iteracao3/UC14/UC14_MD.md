@startuml
skinparam classAttributeIconSize 0
hide methods
left to right direction

class Plataforma {
  -String designacao
}

class Colaborador {
  -String nome
  -String funcao
  -String telefone
  -String email
}

class Tarefa {
  -String referencia
  -String designacao
  -String descInformal
  -String descTecnica
  -Integer duracaoEst
  -Double custoEst
}

interface TipoRegimento{
    -String designacao
    -String descricaoRegras
}

class TarefaPublicada{
    -Date dtInicioPublicitacao
    -Date dtFimPublicitacao
    -Date dtInicioCandidatura
    -Date dtFimCandidatura
    -Date dtInicioSeriacao
    -Date dtFimSeriacao
    -String id
    -Date dataInicioDecisao
    -Date dataFimDecisao
}

class Organizacao {
  -String nome
  -String NIF
  -String website
  -String telefone
  -String email
}

class Candidatura{
  -Date dataFimCandidatura
  -Double valorPretendido
  -Int numDias
  -String txtApresentacao
  -String txtMotivacao
}

class Freelancer{
  -String nome
  -String NIF
  -String telefone
  -String email
}

class Utilizador{
  -String nome
  -String email
  -String password
}

class ProcessoAdjudicacao{
  -String numSequencial
  -Data dataAtribuicao
  -Data inicioRealizacaoTarefa
  -Data fimRealizacaoTarefa
  -Double valorRemunerado
}

class Adjudicacao{
  -Boolean atribuido
}

Plataforma "1" --> "*" TarefaPublicada : publicita >
Plataforma "1" --> "*" TipoRegimento : suporta >
Plataforma "1" --> "*" Organizacao : tem registadas >
Plataforma "1" --> "*" Freelancer : possui >

Organizacao "1" --> "*" Tarefa: possui >
Organizacao "1" --> "1..*" Colaborador: tem >
Organizacao "1" --> "1" Colaborador: tem gestor >

PublicacaoTarefa "0..1" --> "1" Tarefa: publicita >
PublicacaoTarefa "0..1" --> "1" Tarefa: dá origem <
PublicacaoTarefa "*" --> "1" TipoRegimento: rege-se por >
PublicacaoTarefa "*" --> "1" Colaborador: publicado por >
PublicacaoTarefa "1" --> "*" Candidatura: recebe >

ProcessoAdjudicacao "1" --> "1" TarefaPublicada: refere-se a >
ProcessoAdjudicacao "*" --> "1" Colaborador: feito por gestor >
ProcessoAdjudicacao "1" --> "1" Tarefa: refere-se a >
ProcessoAdjudicacao "1" --> "1" Adjudicacao: resulta >
ProcessoAdjudicacao "*" --> "1" Freelancer: refere-se a >
ProcessoAdjudicacao "*" --> "1" Organizacao: refere-se a >
ProcessoAdjudicacao "*" --> "1" TipoRegimento: guia-se por >

Colaborador "1" --> "*" Tarefa: cria >
Colaborador "0..1" --> "1" Utilizador: atua como >

Candidatura "*" --> "1" Freelancer: faz <

Freelancer "0..1" --> "1" Utilizador: atua como >

Adjudicacao "1" --> "1" Candidatura: refere-se a >
@enduml
