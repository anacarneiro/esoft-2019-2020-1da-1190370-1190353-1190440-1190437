@startuml
Class Plataforma {
-String Designacao
}
Class Administrativo {
  -String nome
  -String funcao
  -String telefone
  -String email
}
Class CompetenciaTecnica {
-String nome
-String descBreve
-String descDetalhada

}
Class Formacao {
-String designação do curso
-String instituição 
-int grau
-int média de curso
}
Class Freelancer {
-String nome
-String NIF
-String contacto telefónico 
-String email
}


Plataforma - Freelancer : tem registados/as >
Freelancer -- EnderecoPostal : localizado em >
class EnderecoPostal {
 -String local
 -String codPostal
 -String localidade
}
Freelancer - CompetenciaTecnica : possui >
(Freelancer, CompetenciaTecnica).GraudeProficiencia

Class GraudeProficiencia {
-int grau de proficiencia
}
Freelancer - Formacao : possui >
Freelancer - Administrativo : registado/a por >
hide methods
@enduml