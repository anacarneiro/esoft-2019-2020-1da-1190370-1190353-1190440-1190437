@startuml
class Tarefa {
  -String refUnica
  -String designacao
  -String descInfo
  -String descTacnica
  -String duracao
  -String custo
  Tarefa(String refUnica, String designacao, String dsInfo, String dsTec, String duracao, String custo,CategoriaTarefa categoria)
}

class Organizacao {
  -String nome
  -String NIF
  -String website
  -String telefone
  -String email

  Tarefa novaTarefa(String refUnica, String designacao, String descInfo, String descTecnica, String duracao, String custo, String catID)
  validaTarefa(Tarefa tarefa)
  registaTarefa(Tarefa tarefa) 
  - addTarefa(Tarefa tarefa) 
}
class Plataforma {
  getCategorias()
  getCategoriasPorID (catID)
}

class EspecificarTarefaController {
  novaTarefa(String refUnica, String designacao, String dsBreve, String dsDet, String duracao, String custo, String catID)
  registaTarefa() 
  getCtaregorias()
}

class EspecificarTarefaUI {
  
}
class CategoriaTarefa{
 -String descricao
 -AreaAtividade area
}

EspecificarTarefaUI ..> EspecificarTarefaController
EspecificarTarefaController ..> Organizacao
EspecificarTarefaController ..> Tarefa
Organizacao "1" --> "*" Tarefa : publica
Tarefa ..> CategoriaTarefa : insere-se
EspecificarTarefaController ..> Plataforma 
Tarefa "*" --> Plataforma
@enduml