/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Bruna
 */
public class Reconhecimento {
    
    private String designacao;
    private String data;
    private CompetenciaTecnica competencia;
    private GrauProficiencia grau;

    public Reconhecimento() {
    }

    public Reconhecimento(String designacao, String data) {
        this.designacao = designacao;
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public CompetenciaTecnica getCompetencia() {
        return competencia;
    }

    public void setCompetencia(CompetenciaTecnica competencia) {
        this.competencia = competencia;
    }

    public GrauProficiencia getGrau() {
        throw new UnsupportedOperationException("not implement yet");
    }
    

}
