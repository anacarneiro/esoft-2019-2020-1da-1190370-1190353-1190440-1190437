@startuml
actor Administrativo
participant ":Sistema"
activate Administrativo
Administrativo -> ":Sistema": nova categoria de tarefa
activate ":Sistema"
":Sistema" --> Administrativo: solicita dados(descrição)
deactivate ":Sistema"
Administrativo -> ":Sistema": introduz os dados solicitados
activate ":Sistema"
":Sistema" --> Administrativo: mostra a lista de áreas de atividade
deactivate ":Sistema"
Administrativo -> ":Sistema": seleciona área de atividade
activate ":Sistema"
Administrativo <-- ":Sistema": mostra a lista de competências técnicas
deactivate ":Sistema"
loop
Administrativo -> ":Sistema": seleciona competência técnica
activate ":Sistema"
":Sistema" --> Administrativo: solicita obrigatoriedade(S/N)
Administrativo -> ":Sistema": introduz informacão pedida
end
":Sistema" --> Administrativo: apresenta dados e solicita confirmação
@enduml