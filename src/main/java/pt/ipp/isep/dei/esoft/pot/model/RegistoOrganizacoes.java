/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

/**
 *
 * @author User
 */
public class RegistoOrganizacoes {
    private AutorizacaoFacade m_oAutorizacao;
    private Set<Colaborador> m_lstColaboradores = new HashSet<Colaborador>();
    private final Set<Organizacao> m_lstOrganizacoes;
    private String m_strDesignacao;
     public RegistoOrganizacoes (String strDesignacao)
             
    {
        if ( (strDesignacao == null) ||
                (strDesignacao.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strDesignacao = strDesignacao;
      
        
        this.m_oAutorizacao = new AutorizacaoFacade();
        
        this.m_lstOrganizacoes = new HashSet<>();
   
    }
        public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite,String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor)
    {
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd)
    {
        if (this.validaOrganizacao(oOrganizacao,strPwd))
        {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor,strEmailGestor, strPwd, 
                    new String[] {Constantes.PAPEL_GESTOR_ORGANIZACAO,Constantes.PAPEL_COLABORADOR_ORGANIZACAO}))
                return addOrganizacao(oOrganizacao);
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao)
    {
        return m_lstOrganizacoes.add(oOrganizacao);
    }
    
    public boolean validaOrganizacao(Organizacao oOrganizacao,String strPwd)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail()))
            bRet = false;
        if (strPwd == null)
            bRet = false;
        if (strPwd.isEmpty())
            bRet = false;
        //
      
        return bRet;
    }

       //Organização associada ao Email dado 
    public Organizacao getOrganizacaoUtilizadorAtual(String email) {
        for(Organizacao m_Organizacao : this.m_lstOrganizacoes)
        {
            if (m_Organizacao.hasEmail(email))
            {
                return m_Organizacao;
            }
        }
        
        return null;
    }
        public boolean validaColaborador(Colaborador oColaborador)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        //
      
        return bRet;
    }
            public boolean registaColaborador(Colaborador oColaborador)
    {
        
        
        if (this.validaColaborador(oColaborador))
        {
            
            this.m_oAutorizacao = new AutorizacaoFacade();
            String strNome = oColaborador.getNome();
            String strEmail = oColaborador.getEmail();
            String strPwd = oColaborador.gerarPassword();
            if (m_oAutorizacao.registaUtilizadorComPapel(strNome,strEmail, strPwd,Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            return addColaborador(oColaborador);
        
        }
        return false;
    }
            
            private boolean addColaborador(Colaborador oColaborador)
    {
        
        return m_lstColaboradores.add(oColaborador);
    }
                public AutorizacaoFacade getAutorizacaoFacade()
    {
        return this.m_oAutorizacao;
    }
    public Organizacao getOrganizacaoByEmailUtilizador(String email){
        return null;
    }
    public String getEmailOrg(Organizacao org){
        return null;
    }
}

