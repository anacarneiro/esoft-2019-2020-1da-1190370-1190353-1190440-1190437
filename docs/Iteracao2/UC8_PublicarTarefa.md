# UC8 - Publicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a publicação de uma nova tarefa. O sistema pede os dados necessários (i.e. os diferentes períodos e tipos de regimentos).
O colaborador de organização insere os diferentes dados necessários no sistema. O sistema apresenta os dados e pede confirmação dos mesmos.
O colaborador de organização confirma os dados e o sistema informa do sucesso da publicação da tarefa.

### SSD
![UC8_SSD.svg](UC8_SSD.svg)

### Formato Completo

#### Ator Principal

* Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende publicar tarefas.
* **Organização:** pretende que os seus colaboradores  possam especificar tarefas para posterior publicação.
* **Freelancer:** pretende conhecer as tarefas a que pode candidatar-se.
* **T4J:** pretende a adjudicação de tarefas a freelancers.


#### Pré-condições
*

### Pós-condições
*

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a publicação de uma nova tarefa.
2. O sistema pede os dados necessários (i.e. os diferentes períodos e tipos de regimentos).
3. O colaborador de organização introduz os diferentes dados necessários no sistema.
4. O sistema apresenta os dados e solicita confirmação dos mesmos.
5. O colaborador de organização confirma os dados.
6. O sistema informa o colaborador de organização do sucesso da publicação da tarefa.

#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da publicação da tarefa.

> O caso de uso termina.

5a. O colaborador de organização não confirma os dados.
>       1. O sistema pede novos dados.
>       2. O sistema permite a introdução dos novos dados.
>
        > 2a. O colaborador de organização não altera os dados. O caso de uso termina.

5b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) já existem no sistema e devem ser únicos.
>       1. O sistema alerta o colaborador de organização sobre o sucedido.
>       2. O sistema permite a sua alteração (passo 3).
>
        > 2a. O colaborador de organização não altera os dados. O caso de uso termina.

5c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
>       1. O sistema alerta o colaborador de organização do sucedido.
>       2. O sistema permite a sua alteração (passo 3).
>
        > 2a. O colaborador de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Podem existir organizações com o mesmo NIF? Ou com o mesmo endereço de correio eletrónico?
* A categoria de tarefa pode indicar qualquer competência técnica como sendo obrigatória  e/ou desejável?
* A estimativa de duração de uma tarefa é indicada em dias?
* A informação monetária é indicada em POTs?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC8_MD.svg](UC8_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a publicação de uma nova tarefa.  |	... interage com o utilizador? | PublicarTarefaUI |  Pure Fabrication: pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
| |	... coordena o UC? | PublicarTarefaController | Controller |
| |	... cria instância de Anuncio? | RegistarAnuncio | High Cohesion e Low Coupling |
| 2. O sistema apresenta a lista de tarefas para publicar |	| | |
| 3. O colaborador de organização escolhe uma Tarefa | ... guarda a Tarefa escolhida? | RegistoOrganizacao | High Cohesion e Low Coupling |
| 4. O sistema mostra a lista dos regimentos disponiveis |	... guarda os regimentos? |  Plataforma | |
| 5. O colaborador de organização escolhe o regimento | ... guarda o regimento escolhido?	| Plataforma | |
| 6. O sistema pede os dados necessários (i.e. os diferentes períodos e tipos de regimentos) | | | |  	
| 7. O colaborador de organização introduz os diferentes dados necessários no sistema. |	...guarda os dados introduzidos?	| Anuncio | Information Expert |
| 8. O sistema valida e mostra os dados pedindo confirmação dos mesmos. |	...valida os dados do Anuncio? (validação local) |  Anuncio  | IE: Possui os seus próprios dados |
| |  ...valida os dados do Anuncio? (validação global) |  RegistarAnuncio | High Cohesion e Low Coupling |
| 9. O colaborador de organização confirma. | | | |
| 10. O sistema informa o colaborador de organização do sucesso da publicação da tarefa. | ...guarda o Anuncio criado? | RegistarAnuncio | HighCohesion e Low Coupling |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Organizacao
 * Plataforma
 * Tarefa


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * PublicarTarefaUI  
 * PublicarTarefaController

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

###	Diagrama de Sequência

![UC8_SD.svg](UC8_SD.svg)

###	Diagrama de Classes

![UC8_CD.svg](UC8_CD.svg)
