# UC 10 - Seriar (Não Automaticamente) Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve 

O colaborador de organização inicia o processo não automático de seriação dos candidatos à realização de um anúncio. O sistema mostra os anúncios publicados pelo colaborador em fase de seriação não automática e que ainda não foram seriadas e pede-lhe para escolher um. O colaborador seleciona um anúncio. O sistema mostra as candidaturas existente e solicita a sua classificação. O colaborador classifica as candidaturas. O sistema mostra os colaboradores da organização e pede para selecionar os outros participantes no processo. O colaborador seleciona. O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador confirma. O sistema regista os dados juntamente com a data/hora atual, de seguida inicia o processo de atribuição, se a atribuição for obrigatória, e informa o colaborador do sucesso da operação

![UC10-SSD.svg](UC10_SSD.svg)

### Formato Completo

#### Ator principal

* Colaborador de Organização

#### Partes interessadas e seus interesses

* *Colaborador de Organização:* pretende seriar as candidaturas que um anúncio recebeu.
* *Freelancer:* pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* *Organização:* pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* *T4J:* pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições
* Existir pelo menos um anúncio de tarefa em condições de ser seriado manualmente pelo colaborador ativo no sistema.

#### Pós-condições
* A informação do processo de seriação é registada no sistema.
* A atribuição deve ser realizada e registada, caso seja obrigatória.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador inicia o processo não automático de seriação das candidaturas a um anúncio.
2. O sistema mostra os anúncios publicados pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.
3. O colaborador seleciona um anúncio.
4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas, a sua classificação e justificação.
5. O colaborador seleciona uma candidatura, indica a classificação e justifica-a.
6. Os passos 4 e 5 repetem-se até que estejam classificadas todas as candidaturas.
7. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.
8. O colaborador seleciona um colaborador.
9. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.
10. O sistema solicita um texto de conclusão.
11. O colaborador introduz o texto.
12. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.
13. O colaborador confirma.
14. O sistema regista os dados juntamente com a data/hora atual, de seguida inicia o processo de atribuição, se a atribuição for obrigatória, e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

\*a. O colaborador solicita o cancelamento do processo de seriação das candidaturas.
O caso de uso termina.


2a. A lista de anúncios encontra-se vazia.
1. O sistema alerta para o facto. O caso de uso termina.


4a. A lista de candidaturas encontra-se vazia.
1. O  sistema alerta para o facto. O caso de uso termina.


7a. O colaborador não indica todas as classificações.
1. O sistema informa quais as candidaturas que faltam classificar.
2. O sistema permite a classificação das candidaturas em falta (passo 7).
> 2a. O colaborador não classifica. O caso de uso termina.


10a. Não existem mais colaboradores passíveis de serem adicionados.
1. O sistema alerta para o facto. O caso de uso continua (passo 12).


12a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 2)
>> 2a. O colaborador não altera os dados. O caso de uso termina.

14a. A atribuição não é  obrigatória.
O caso de uso termina.


#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Deve o freelancer ser notificado do processo de seriação e atribuição? Se sim, como?
*  Qual é a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.scg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:----------------------------|
|1. O colaborador inicia o processo não automático de seriação das candidaturas a um anúncio.|... interage com o utilizador?|SeriarAnuncioUI|Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio|
||... coordena o UC?|SeriarAnuncioController|Controller|
||... conhece o utilizador?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores|
||... sabe a que organização o utilizador/gestor pertence?|RegistoOrganizacoes|IE + padrão HC e LC: no MD a Plataforma tem registadas Organizacao, mas ficará ao cargo do RegistoOrganizacoes agregar e gerir as instâncias de Organizacao|
|||ListaColaboradores|IE + padrão HC e LC: no MD a Organização tem Colaborador, mas ficará ao cargo da ListaColaboradores agregar e gerir as instâncias de Colaborador|
|||Colaborador|IE: conhece os seus dados (e.g. email)|
||... conhece o tipo de regimento aplicável?|TiposRegimento|IE + padrão HC e LC: no MD plataforma suporta TipoRegimento, mas delega as suas responsabilidades na classe "TiposRegimento"|
||... conhece as regras de seriação do anúncio?|TipoRegimento|IE: conhece os seus próprrios dados|
||... cria a instância do ProcessoSeriacao?|Anuncio|Creator (Regra3): No MD anúncio espoleta ProcessoSeriacao|
|2. O sistema mostra os anúncios publicados pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.|... conhece os anúncios publicados pelo colaborador?|RegistoAnuncios|IE + padrão HC e LC: no MD a platafoma publicita anúncios, mas delega as suas responsabilidades na classe RegistoAnuncios|
|3. O colaborador seleciona um anúncio.|... conhece os dados do anúncio seleccionado?|Anuncio|IE: conhece os seus próprios dados|
|4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas, a sua classificação e justificação.|... conhece as candidaturas recebidas pelo anúncio?|ListaCandidaturas|IE + padrão HC e LC: no MD anúncio recebe candidaturas, no entanto delega as suas responsabilidades na classe "ListaCandidaturas"|
|5. O colaborador seleciona uma candidatura, indica a classificação e justifica-a.|... conhece os dados da candidatura?|Candidatura|IE: conhece os seus próprios dados|
||... guarda os dados da seriação?|ProcessoSeriacao|IE: no MD processo de seriação tem como resultado uma classificação|
||... cria a instância de Classificacao?|ProcessoSeriacao|Creator (Regra4): o processo de seriação implica classificação de candidaturas|
||... guarda os dados da classificação?|Classificacao|IE: conhece os seus próprios dados|
|6. Os passos 4 e 5 repetem-se até que estejam classificadas todas as candidaturas.||||
|7. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.||||
|8. O colaborador seleciona um colaborador.||||
|9. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.||||
|10. O sistema solicita um texto de conclusão.||||
|11. O colaborador introduz o texto.||||
|12. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.|... valida os dados da seriação? (validação local)|ProcessoSeriacao|IE: conhece os seus próprios dados|
||... valida os dados da seriação? (validação global)|Anuncio|IE: anúncio espoleta e agrega a seriação|
|13. O colaborador confirma.||||
|14. O sistema regista os dados juntamente com a data/hora atual, de seguida realiza a atribuição, se obrigatória, e informa o colaborador do sucesso da operação.|... guarda a seriação?|Anuncio|IE: no MD anúncio espoleta a seriação|
||... conhece a tarefa a adjudicar?|Anuncio|IE: no MD anuncio é referente a uma tarefa|
||... conhece os dados da tarefa a adjudicar?|Tarefa|IE: possui os seus próprios dados|
||... conhece o freelancer, remuneração e número de dias necessários à realização da tarefa?|Candidatura|IE: conhece os seus próprios dados|
||... cria a instância ProcessoAtribuicao?|Anuncio|Creator (Regra3): o anuncio conhece os dados necessário para realizar a atribuicao da tarefa|
||... guarda os dados da atribuição?|ProcessoAtribuicao|IE: conhece os seus próprios dados|
||... guarda a atribuição?|Anuncio|IE: no MD o processo de atribuição é referente a um anuncio|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * ProcessoAtribuicao
 * TipoRegimento
 * Classificacao
 * Organizacao
 * Colaborador
 * Adjudicacao


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoAnuncios
 * RegistoOrganizacoes
 * ListaColaboradores
 * ListaCandidaturas



###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)

UC10_SD_atribuirCandidaturasSubmetidas


![UC10_SD_atribuirCandidaturaSubmetida.svg](UC10_SD_atribuirCandidaturaSubmetida.svg)

###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)

*Nota:* Algumas dependências estão omitidas.