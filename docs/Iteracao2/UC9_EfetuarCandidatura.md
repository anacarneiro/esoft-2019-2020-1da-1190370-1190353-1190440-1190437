# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia o processo de candidatura a uma dada tarefa. O sistema **apresenta** os anúncios com tarefas **elegíveis** para o Freelance(tem que possuir o g.p.m. para a tarefa). O freelancer **seleciona** o anúncio ao qual se pretende candidatar. O sistema solicita alguns dados(i.e valor pretendido pela realização da tarefa, número de dias para realizar a mesma e um  texto de apresentação e/ou motivação (opcional)). O freelancer introduz os dados solicitados. O sistema **valida** e apresenta os dados ao freelancer, pedindo que os confirme. O freelancer confirma. O sistema **regista a candidatura** e informa o freelancer do sucesso da operação.


### SSD
![UC9-SSD](UC9_SSD.svg)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses

* **Organização:** pretende ter vários candidatos para realizarem as suas tarefas.
* **Freelancer:** pretende candidatar-se a uma tarefa.
* **TJ4:** pretende que os freelancers se possam candidatar aos anúncios das organizações.

#### Pré-condições

* Existir anúncios para que os freelancer se possam candidatar.

#### Pós-condições

* O freelancer passa a ser candidato à execução da tarefa proposta pelo anúncio que escolheu.
* O freelancer pode ou não ser selecionado para executar a tarefa.

#### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia o processo de candidatura a uma dada tarefa.
2. O sistema apresenta os anúncios com tarefas elegíveis para o Freelance(tem que possuir o g.p.m. para a tarefa).
3. O freelancer seleciona o anúncio ao qual se pretende candidatar.
4. O  sistema solicita alguns dados(i.e valor pretendido pela realização da tarefa, número de dias para realizar a mesma e um  texto de apresentação e/ou motivação (opcional)).
5.  O freelancer introduz os dados solicitados.
6. O sistema valida e apresenta os dados ao freelancer, pedindo que os confirme.
7. O freelancer confirma.
8. O sistema regista a candidatura e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da candidatura ao anúncio.

> O caso de uso termina.

*b. A plataforma não tem nenhum anúncio elegível para o freelancer.
> 1. O sistema informa quais os dados em falta.
> 2. O caso de uso termina.

3a. O sistema deteta que o freelancer já se candidatou ao anúncio em questão.

> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	4a. O gestor de organização não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o gestor de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 4a. O gestor de organização não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados exceto o texto de apresentação e/ou motivação são obrigatórios?
* Qual ou quais os dados que associam um freelancer a um anúncio/tarefa?
* Um freelancer pode candidatar-se a mais que um anúncio?
* Se o freelancer se poder candidatar a vários anúncios existe algum limite?
* Se o freelancer se poder candidatar a vários anúncios pode ser escolhido para realizar mais que 1 anúncio?
* Como é que o freelancer é informado se foi escolhido ou não?
* Depois de ter concluído uma candidatura é possível anular?
* Caso o freelancer seja selecionado para executar 1 tarefa pode rejeita-la?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC9-MD](UC9_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia o processo de candidatura a uma dada tarefa | ...interage com o utilizador? | EfetuarCandidaturaUI | Pure Fabrication |
|  | ...coordena o UC? | EfetuarCandidaturaController | Controller |
|  | ...cria a instância da candidatura? | Anuncio | Creator |
|  | ...conhece o utilizador a usar o sistema? | SessãoUtilizador | Information Expert (IE) |
| 2. O sistema apresenta os anúncios com tarefas elegíveis para o Freelance | ...verifica para quais anuncios o freelancer é elegível? | RegistoAnuncio | Information Expert (IE): possui todos os anúncios |
|  | lista os anúncios? | RegistoAnuncio | Information Expert (IE): possui a lista |
|  | ...possui o ResgistoAnuncio? | Plataforma | IE: a Plataforma possui o RegistoAnuncio |
| 3. O freelancer seleciona o anúncio ao qual se pretende candidatar |  |  |  |
| 4. O  sistema solicita alguns dados(i.e valor pretendido pela realização da tarefa, número de dias para realizar a mesma e um  texto de apresentação e/ou motivação (opcional)) |  | |  |
| 5.  O freelancer introduz os dados solicitados| ...guarda os dados introduzidos? | Candidatura | IE: instância criada no passo 1 |
| 6. O sistema valida e apresenta os dados ao freelancer, pedindo que os confirme | ...valida os dados? (validação local) | Candidatura | IE: a candidatura possui as suas próprias verificações |
|  | ...valida os dados? (validação global) | RegistoCandidatura | HC+LC: a RegistoCandidatura possui todas as candidaturas para o respetivo anúncio |
| 7. O freelancer confirma |     |  |  |
| 8. O sistema regista a candidatura e informa o freelancer do sucesso da operação |    ...regista a candidatura? | RegistoCandidatura |HC+LC: A candidatura é associada ao anúncio |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * Freelancer

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EfetuarCandidaturaUI  
 * EfetuarCandidaturaController
 * RegistoAnuncio
 * RegistoCandidatura

 Outras classes de sistemas/componentes externos:

  * SessaoUtilizador
  * AplicacaoPOT

###	Diagrama de Sequência Principal

![UC9_SD](UC9_SD.svg)

####	Diagrama de Sequência Referência (getListaAnunciosElegiveis)

![UC9_SD_getListaAnunciosElegiveis](UC9_SD_getListaAnunciosElegiveis.svg)



###	Diagrama de Classes

![UC9-CD](UC9_CD.svg)
