/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author Bruna
 */
public class GrauProficiencia {
    
    private String designacao;
    private int valor;

    public GrauProficiencia() {
    }

    public GrauProficiencia(String designacao, int valor) {
        this.designacao = designacao;
        this.valor = valor;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
    
    
}
