@startuml
class Plataforma {
-String designação
}
Categoria - Tarefa : está associada a >
Plataforma - Categoria : possui >
class Categoria {
-String identificador
-String descBreve
-ArrayList<String> competencias
-String areaAtividade
}
Plataforma - CompetenciaTecnica : possui >

CompetenciaTecnica - AreaAtividade: referente a >
class CompetenciaTecnica {
-String codigo
-String descBreve
-String descDetalhada
}
Categoria "0..*" - "1..*" CompetenciaTecnica
(Categoria, CompetenciaTecnica) .. Obrigatoria
class Obrigatoria {
-String tipoObrig
}


class AreaAtividade {
-String codigo
-String descBreve
-String descDetalhada
}
class Tarefa {
-String referência
-String designação
-String descricaoInformal
-String descriçaoTecnica
-String duracaoEst
-String custoEst
}
hide methods
@enduml