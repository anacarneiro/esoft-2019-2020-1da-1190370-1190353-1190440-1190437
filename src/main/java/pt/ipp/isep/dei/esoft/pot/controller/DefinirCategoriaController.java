/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.ArrayList;
import java.lang.String;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Categoria;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;


/**
 *
 * @author 35193
 */
public class DefinirCategoriaController {
    private Plataforma m_oPlataforma;
    private Categoria m_oCategoria;

    public DefinirCategoriaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
     public boolean novaCategoria(String strDescricao, String strIdentificador, String strAreaAtividade, ArrayList<String> strCompetencias)
    {
        try
        {
            this.m_oCategoria = this.m_oPlataforma.novaCategoria(strDescricao, strIdentificador, strAreaAtividade, strCompetencias);
            return this.m_oPlataforma.validaCategoria(this.m_oCategoria);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCategoria = null;
            return false;
        }
    }
         public boolean registaCategoria()
    {
        return this.m_oPlataforma.registaCategoria(this.m_oCategoria);
    }

    public String getCategoriaString()
    {
        return this.m_oCategoria.toString();
    }


  public List<AreaAtividade> getAreasAtividade()
    {
        return this.m_oPlataforma.getAreasAtividade();
    }
  public List<CompetenciaTecnica> getCompetencias()
  {
      return this.m_oPlataforma.getCompetencias();
  }
    
}
   