@startuml
autonumber
actor "Administrativo" as ADM

activate ADM
ADM -> ":Sistema" : especifica uma competência técnica
activate ":Sistema"
":Sistema" --> ADM : solicita dados \n(código, descrição breve e detalhada)
deactivate ":Sistema"

ADM -> ":Sistema" : introduz os dados solicitados
activate ":Sistema"
":Sistema" --> ADM : apresenta a lista das áreas de atividade das quais se irá selecionar uma
deactivate ":Sistema"

ADM -> ":Sistema" : seleciona uma área de atividade
activate ":Sistema"
":Sistema" --> ADM : apresenta os dados ao administrativo, pedindo que os confirme
deactivate ":Sistema"

ADM -> ":Sistema" : confirma
activate ":Sistema"
":Sistema" --> ADM : resgista os dados e informa o administrativo do sucesso da operação
deactivate ":Sistema"

deactivate ADM

@enduml
