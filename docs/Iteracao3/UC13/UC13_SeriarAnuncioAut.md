# UC 13 - Seriar (automaticamente) Candidaturas de Anúncios

## 1. Engenharia de Requisitos

### Formato Breve

O Tempo/Relógio inicia o processo de seriação e atribuição automática das candidaturas (diariamente numa hora pré-definida). O sistema identifica e seleciona os anúncios seriáveis, ou seja, que possuem critérios objetivos, que a seriação está especificada pelo regimento como sendo obrigatória, que estejam em período de seriação e que não tenham sido ainda seriados. O sistema inicia o processo de seriação e atribuição automática dos anúncios selecionados. O sistema regista os dados e termina e processo.

### SSD
![UC13_SSD](UC13_SSD.svg)


### Formato Completo

#### Ator principal

* Tempo/Relógio

#### Partes interessadas e seus interesses

* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições

\-

#### Pós-condições

* A informação do processo de seriação é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Tempo/Relógio inicia o processo de seriação e atribuição automática das candidaturas (diariamente numa hora pré-definida).
2. O sistema identifica e seleciona os anúncios seriáveis (que possuem critérios objetivos, que a seriação está especificada pelo regimento como sendo obrigatória, que estejam em período de seriação e que não tenham sido ainda seriados). O sistema inicia o processo de seriação e atribuição automática dos anúncios selecionados. O sistema regista os dados e termina e processo.


#### Extensões (ou fluxos alternativos)

2a. Não existem anúncios em período de seriação
> O caso de uso termina.

2b. Todos os anúncios já se encontram seriados
> O caso de uso termina.

2c. Não existem anúncios com critérios de seriação objetivos
> O caso de uso termina.

2d. Não existem anúncios com atribuição obrigatória
> O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência

* Diáriamente

#### Questões em aberto

* O processo de seriação automática pode ser interrompido? Como?
* Em caso de 2 ou mais candidaturas preencherem os critérios da mesma maneira, qual o critério de desempate?
* Existem anúncios/tarefas que aceitam mais que 1 freelancer para a realização do trabalho?
* Pode haver desclassificação de candidaturas? Qual/Quais os motivos?
* É possível mudar o horário a que a seriação automática é feita?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC13_MD.svg](UC13_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Tempo/Relógio inicia o processo automático de seriação das candidaturas quando chega ao horário estabelecido. |	interage com o timer? | SeriarCandidaturaAnuncioTask | Controller |
| 2. O sistema identifica e seleciona os anúncios seriáveis (que possuem critérios objetivos e que a seriação está especificada pelo regimento como sendo obrigatória) que estejam em período de seriação e que não tenham sido ainda seriados. O sistema inicia o processo de seriação e atribuição automática dos anúncios selecionados. O sistema regista os dados e termina e processo. | conhece o Anuncio? | RegistoAnuncio | HC+LC delega a RegistoAnuncios |
| | quem conhece os dados do anuncio? | Anuncio | IE: O Anuncio conhece os seus dados |
| | quem é guarda os dados da seriação? | ProcessoSeriacao | IE: O ProcessoSeriação conhece os seus dados |
| |quem é que guarda os dados da adjudicação? | ProcessoAtribuicao | IE: O ProcessoAtribuição conhece os seus dados |
| | cria o agendador? |Plataforma | Creator (regra 1)  |
| | cria a atribuição? | SeriarCandidaturaAnuncioTask | Controla o agendamento |
| | ordena as candidaturas segundo o critério correto? | ProcessoSeriacao | Possui o anúncio e é responsável pela seriação |
| | conhece as candidaturas? |ListaCandidaturas | HC+LC delega a ListaCandidaturas |
| | cria a instância ProcessoSeriacao? |Anuncio | Anuncio possui ProcessoSeriacao |
| | cria a instância Classificação? | Classificacao | Creator |
| | guarda o classificacao? |ProcessoSeriacao | ProcessoSeriacao possui as Classificacoes |
| | conhece a tarefa? | Anuncio | Possui Tarefa |
| | conhece a descrição informal da tarefa? | Tarefa | Creator (regra 1) |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * SeriarCandidaturaAnuncioTask
 * RegistoAnuncio
 * Anuncio
 * ProcessoSeriacao
 * ProcessoAtribuicao
 * Plataforma
 * ListaCandidaturas
 * Classificacao
 * Tarefa
 * Freelancer

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarCandidaturaAnuncioTaskController


###	Diagrama de Sequência

![UC13_SD.svg](UC13_SD.svg)

![UC13_SD_getProcessoSeriacao.svg](UC13_SD_getProcessoSeriacao.svg)

![UC13_SD_getProcessoAtribuicao.svg](UC13_SD_getProcessoAtribuicao.svg)


###	Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)
