# UC 11 - Atualizar Candidatura Submetida

## 1. Engenharia de Requisitos
2z<3
### Formato Breve

O Freelancer inicia a atualização de uma candidatura. O sistema solicita a escolha da candidatura entre todas as candidaturas submetidas pelo Freelancer (que ainda estão na dentro da data dinal de candidatura) em todos os anúncios. O Freelancer identifica a candidatura que quer atualizar. O sistema solicita os dados necessários para atualizar a candidatura (i.e. valor pretendido, numero dias, texto apresentacão (opcional), texto motivação (opcional)). O Freelancer introduz os dados solicitados. O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmação. O Freelancer confirma os dados. O sistema verifica mais uma vez que a data limite não foi ultrapassada. O sistema atualiza finalmente a candidatura.

### SSD
![UC11-SSD](UC11_SSD.png)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende receber Candidaturas aos Anúncios publicados.
* **Organização:** pretende receber Candidaturas para as Tarefas publicadas para execução por Freelancers.
* **Freelancer:** pretende ter a possiblilidade de alterar as candidaturas previamente submetidas.
* **T4J:** pretende receber Candidaturas para posterior atribuição das tarefas a Freelancers.

#### Pré-condições

* O Freelancer submeteu pelo menos uma candidatura a um determinado anuncio, e está dentro do limite para atualizar a candidatura.

#### Pós-condições

* É atualizada uma candidatura submetida pelo freelancer.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Freelancer inicia a atualização da candidatura.
2. O sistema solicita a escolha da candidatura entre todas as candidaturas submetidas pelo Freelancer (que ainda estão na dentro da data final de candidatura) em todos os anúncios.
3. O Freelancer identifica a candidatura que deseja atualizar.
4. O sistema solicita os dados necessários para a candidatura ao anúncio (i.e. valor pretendido, numero dias, texto apresentacão(opcional), texto motivação(opcional)).
5. O Freelancer introduz os dados solicitados.
6. O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmação.
7. O Freelancer confirma os dados.
8. O sistema atualiza a candidatura ao anúncio e informa o Freelancer do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O Freelancer solicita o cancelamento da atualização da candidatura a anúncio.
> O caso de uso termina.

2a. Não existem anúncios em período de candidatura.
> O caso de uso termina.

8a. O periodo de candidatura acabou.
>O sistema avisa o Freelancer do sucedido.
>O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 4)
>
	> 2a. O freelancer não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
>   1. O sistema alerta o Freelancer para o facto.
>   2. O sistema permite a sua alteração (passo 5).
>
	> 2a. O Freelancer não altera os dados. O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto
\-

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC11-MD](UC11_MD.png)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O Freelancer inicia a atualização da candidatura a Anúncio |... interage com o utilizador?|AtualizarCandidaturaUI	|Pure Fabrication|
| |...coordena o UC?	 				| AtualizarCandidaturaController	| Atualizar   |Pure Fabrication|
| |...tem instância de Candidatura? 			| Anúncio | Creator (Regra1): no MD o Anúncio recebe Candidaturas|
| |							| ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| |...conhece o utilizador/Freelancer a usar o sistema?	|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
| |...conhece o Freelancer ?				|Plataforma|conhece todos os Freelancers|
| |				|RegistoFreelancer|Por aplicação de HC+LC delega a RegistoFreelancer|
| 2. O sistema apresenta as candidaturas submetidas pelo Freelancer|... conhece as candidaturas? |RegistoAnuncios|IE: no MD os Anuncios possuem Candidaturas. |
| | 						     | RegistoAnuncios	|IE: no MD a Plataforma possui Anuncios. Por aplicação de HC+LC delega a RegistoAnuncios|
| |...conhece as candidaturas submetidas pelo Freelancer| RegistoAnuncios 	| O Freelancer possui Reconhecimentos de Competências Técnicas | IE |  
| | 						     | Reconhecimento	| O Reconhecimento confirma o GrauProficiência do freelancer numa Competência Técnica | IE |  
| | 						     | Anúncio		| O Anúncio é relativo a uma Tarefa | IE: no MD o Anúncio é relativo a uma Tarefa |
| | 						     | Tarefa		| A Tarefa tem uma Categoria de Tarefa associada | IE: no MD a Tarefa é relativa a uma Categoria de Tarefa |
| | 						     | Categoria | A Categoria de Tarefa possui Carácter das Competências Técnicas | IE: no MD a Categoria de Tarefa possui CarácterCT |
| | 						     | CarácterCT | O Carácter de Competência Técnica possui GrauProficiência minino para cada CT e obrigatoriedade dessa CT | IE: no MD o CarácterCT possui GrauProficiência mínimo e obrigatoriedade|
| | 						     | GrauProficiência | A Competência Técnica possui os Graus de Proficiência para cada Competência Técnica | IE |
| 3. O Freelancer seleciona uma candidatura. | | | |
| 4. O sistema solicita os dados necessários para a candidatura (i.e. valor pretendido, numero dias, texto apresentacão(opcional), texto motivação(opcional)) |N/A|||
| 5. O Freelancer introduz os dados solicitados. | ... guarda os dados introduzidos?|Anúncio| No MD Anúncio recebe Candidaturas|
| |							| ListaCandidaturas | Por aplicação de HC+LC delega a ListaCandidaturas|
| |							| Candidatura | IE: Candidatura conhece os seus dados|
| 6. O sistema valida e apresenta os dados ao Freelancer e pede a sua confirmação.|	... valida os dados da Candidatura (validação local)?|Candidatura| IE: possui os seus próprios dados.|
| |	... valida os dados da Candidatura (validação global)?| ListaCandidaturas| IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| 7. O Freelancer confirma. | N/A|||
| 8. O sistema regista os dados e atualiza a Candidatura e informa o Freelancer do sucesso da operação. |...guarda a Candidatura?| Anúncio|IE: no MD o Anúncio recebe Candidaturas.|
| |							| ListaCandidaturas | IE: no MD o Anúncio recebe Candidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| |...informa o colaborador?|AtualizarCandidaturaUI|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Candidatura
 * Freelancer
 * Anúncio

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AtualizarCandidaturaUI  
 * AtualizarCandidaturaController
 * RegistoFreelancer
 * RegistoAnuncios
 * ListaCandidaturas

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

###	Diagrama de Sequência

![UC11-SD.svg](UC11_SD.svg)

![UC11_SD_getAnunciosElegiveis.svg](UC11_SD_getAnunciosElegiveis.svg)


###	Diagrama de Classes

![UC11_CD.svg](UC11_CD.svg)

