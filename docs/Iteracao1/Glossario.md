# Glossário

**Os termos devem estar organizados alfabeticamente.**

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio.|
| **ADM** | Acrónimo para Administrativo.|
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar um ou mais tarefas (semelhantes).|
| **Colaborador de Organização** | Registado pelo gestor de organização e tem a responsabilidade de especificação de tarefas e listagem de competências facultativas e obrigatórias para o cumprimento das mesmas.|
| **CO** | Acrónimo para colaborador de organização.|
| **descInformal** | Descrição informal pedida na criação de uma nova tarefa.|
| **descTecnica** | Descrição técnica pedida na criação de uma nova tarefa. |
| **catID** | Identificador da categoria de tarefa.|
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.|
| **Gestor de Organização** | Colaborador da organização responsável por especificar na plataforma outros colaboradores nomeadamente o colaborador de organização.|
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma.|
| **Outsourcing**| Processo de contratar alguém externo à organização.
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática.|
| **refUnica** | Referencia unica associada a cada tarefa.|
| **Task for Joe** | Nome da aplicação/plataforma.|
| **T4J** | Acrónimo de "Task for Joe".|
| **Utilizador** | Pessoa que interage com a aplicação informática.|
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_.|
