# UC12 - Retirar Candidatura Submetida

## 1. Engenharia de Requisitos

### Formato Breve

O Freelancer inicia o retirar de uma candidatura submetida. O sistema  apresenta uma lista de possíveis candidaturas que podem ser eliminadas. O Freelancer escolhe a candidatura que pretende eliminar. O sistema **valida** e apresenta a candidatura escolhida para ser eliminada, pedindo que a confirme. O Freelancer confirma. O sistema **elimina a candidatura** e informa o Freelancer do sucesso da operação.

### SSD
![UC12_SSD.svg](UC12_SSD.svg)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses
* **Freelancer:** pretende retirar candidatura submetida.
* **T4J:** pretende o posível retiro de um candidatura submetida por um freelancer.


#### Pré-condições
* O Freelancer têm que ter efetuado no mínimo uma candidatura para poder iniciar este uc.


#### Pós-condições


### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a eliminação de uma candidatura submetida.
2. O sistema apresenta uma lista de candidaturas elegíveis para serem eliminadas.
3. O Freelancer escolhe uma das candidaturas.
4. O sistema valida e apresenta a candidatura que o freelancer prentende eliminar pedindo que ele confirme.
5. O Freelancer confirma.
6. O sistema elimina a candidatura e informa o Freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O Freelancer solicita o cancelamento da eliminação da candidatura

2a. Não existe Candidaturas
> O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
* A canditura precisa de ser realmente apagada do sistema ou pode ela ser passada para um estado cancelado?
* Qual é a frequência de ocorrência deste caso de uso?

 
## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC12_MD.svg](UC12_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a eliminação de uma candidatura submetida.  | ... interage com o utilizador? | RetirarCandidturaUI |  Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|   | ... coordena o UC? | RetirarCandidaturaController | Controller    |
||... conhece o utilizador?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores|
||... conhece o Freelancer?|RegistoFreelancer|IE + padrão HC e LC: no MD a Plataforma tem registados Freelancers, mas ficará ao cargo do RegistoFreelancers agregar e gerir as instâncias Freelancer|
|   | ... elimina a instancia de candidatura? | ListaCandidaturas | HC+LC: a ListaCandidaturas contém/agrega Candidatura.  |
| 2. O sistema mostra a lista de candidaturas do freelancer . | |             |                              |
||...conhece a lista de candidaturas?| ListaCandidaturas |IE: no MD o Anuncio possui LiataCandidaturas. Por aplicação de HC+LC delega a ListaCandidaturas|
| 3. O freelancer seleciona a candidatura que pretende eliminar. | ...guarda a candidatura selecionada?| Candidatura   | Information Expert (IE)-instância possui os seus próprios dados.     |
| 4. O sistema valida e apresenta os dados pedindo que os confirme.   | ... valida a candidatura a ser eliminada?  |  ListaCandidaturas  | IE: ListaCandidaturas possui todas as instâncias de Candidatura.                             |
| 5. O freelancer confirma os dados. ||||
| 6. O sistema informa o freelancer do sucesso da operação.| ...retira a candidatura da lista de candidaturas? | ListaCandidaturas | IE: a ListaCandidaturas contém/agrega Candidatura.|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

* Plataforma
* Candidatura
* Anuncio


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RetirarCandidaturaUI  
 * RetirarCandidaturaController
 * RegistroFreelancer
 * ListaCandidaturas

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

###	Diagrama de Sequência

![UC12_SD.svg](UC12_SD.svg)

SD_getCandidaturasEligiveisParaEliminarDoFreelancer(freel)

![SD_getCandidaturasEligiveisParaEliminarDoFreelancer.svg](SD_getCandidaturasEligiveisParaEliminarDoFreelancer.svg)


###	Diagrama de Classes

![UC12_CD.svg](UC12_CD.svg)


