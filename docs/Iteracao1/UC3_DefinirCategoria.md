# UC3 - Definir Categoria de Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a definição de uma categoria de tarefa. O sistema solicita os dados necessários (descrição). O administrativo introduz os dados solicitados. O sistema apresenta uma lista de áreas de atividade. O administrador seleciona uma área de atividade. O sistema começa um loop. O sistema apresenta a lista de competencias técnicas. O administrador seleciona uma competência técnica. O sistema solicita a sua obrigatoriedade. O administrador introduz os dados necessários. O administrador termina o loop. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir a categoria de tarefa, a partir da descrição e da área de atividade, escolhendo posteriormente as competencias técnicas e definindo asssim a categoria de tarefa.
* **T4J:** pretende que a plataforma permita catalogar as tarefas em categorias de tarefa dentro de áreas de atividade e contendo competencias técnicas.


#### Pré-condições
Área de Atividade

#### Pós-condições
A informação da categoria de tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma categoria de tarefa. 
2. O sistema solicita os dados necessários (descrição). 
3. O administrativo introduz os dados solicitados. 
4. O sistema valida e apresenta a lista de áreas de atividade.
5. O administrativo seleciona a área de atividade. 
6. O sistema inicia um loop e apresenta ao administrativo a lista de competencias técnicas.
7. O administrativo seleciona a competencia tecnica.
8. O sistema solicita ao administrativo a obrigatoriedade da competencia tecnica.
9. O administrativo introduz os dados necessários e termina o loop.
10. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.
11. O administrativo confirma.
12. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da  área de atividade.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios? 
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.svg](UC3_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O administrativo inicia a definição de uma categoria de tarefa.   | ... interage com o utilizador? | DefinirCategoriaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|		 |	... coordena o UC?	| EspecificarCategoriaController | Controller    |
|  		 |	... cria instância de Categoria| Plataforma   | Creator (Regra1)   |
|2. O sistema solicita os dados necessários (descrição). 		 |             |                              |
|3. O administrativo introduz os dados solicitados.   |   ... guarda os dados introduzidos?  |   Categoria | Information Expert (IE) - instância criada no passo 1     |
|4. O sistema valida e apresenta a lista de áreas de atividade.
|5. O administrativo seleciona a área de atividade. 
|6. O sistema inicia um loop e apresenta ao administrativo a lista de competencias técnicas.
|7. O administrativo seleciona a competencia tecnica.
|8. O sistema solicita ao administrativo a obrigatoriedade da competencia tecnica.
|9. O administrativo introduz os dados necessários e termina o loop.
|10. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.	  |	...valida os dados da Categoria (validação local) | Categoria |                              |IE. Possui os seu próprios dados.|  	
|11. O administrativo confirma.
|12. O sistema regista os dados e informa o administrativo do sucesso da operação.  |	... guarda a Categoria criada? | Plataforma  | IE: No MD a Plataforma possui Categoria| 

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Categoria


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController
 * Obrigatoria


###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)


###	Diagrama de Classes

![UC3_CD.svg](UC3_CD.svg)




