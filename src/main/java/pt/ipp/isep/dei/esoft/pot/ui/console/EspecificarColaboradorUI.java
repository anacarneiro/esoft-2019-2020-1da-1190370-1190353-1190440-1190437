package pt.ipp.isep.dei.esoft.pot.ui.console;
import pt.ipp.isep.dei.esoft.pot.controller.EspecificarColaboradorController;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author Bruna Coutinho
 * @since 02/04/2020
 */
public class EspecificarColaboradorUI {
    private final EspecificarColaboradorController m_controller;
    public EspecificarColaboradorUI()
    {
        m_controller = new EspecificarColaboradorController();
    }

    public void run()
    {
        System.out.println("\nRegistar Colaborador:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaColaborador()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados() {
        String strNome = Utils.readLineFromConsole("Nome do Colaborador: ");
        String strFuncao = Utils.readLineFromConsole("Função: ");
        String strTelefone = Utils.readLineFromConsole("Telefone: ");
        String strEmail = Utils.readLineFromConsole("EMail: ");
        System.out.println("Password gerada automáticamente");
        
        return m_controller.novoColaborador(strNome,strFuncao, strTelefone, strEmail);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\n Informação a Registar:\n" + m_controller.getColaboradorString());
    }
}
