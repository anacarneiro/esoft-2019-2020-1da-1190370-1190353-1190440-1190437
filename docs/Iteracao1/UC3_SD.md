@startuml
actor Administrativo
participant ":DefinirCategoriaUI"
participant ":DefinirCategoriaController"
participant ":Plataforma" 
participant "cat:Categoria"
participant ":Obrigatoria" 
activate Administrativo
Administrativo -> ":DefinirCategoriaUI": nova categoria de tarefa
activate ":DefinirCategoriaUI"
":DefinirCategoriaUI" --> Administrativo: solicita dados(descrição)
deactivate ":DefinirCategoriaUI"
Administrativo -> ":DefinirCategoriaUI": introduz os dados solicitados

activate ":DefinirCategoriaUI"


":DefinirCategoriaUI" -> ":DefinirCategoriaController": getAreasAtividade()
activate ":DefinirCategoriaController"

":DefinirCategoriaController"-> ":Plataforma": getAreasAtividade()
activate ":Plataforma"




":DefinirCategoriaUI" --> Administrativo: mostra a lista de áreas de atividade

deactivate ":DefinirCategoriaUI"
Administrativo -> ":DefinirCategoriaUI": seleciona área de atividade
activate ":DefinirCategoriaUI"
":DefinirCategoriaUI"->":DefinirCategoriaController": getCompetencias()
":DefinirCategoriaController"-> ":Plataforma": getCompetencias()
deactivate ":Plataforma"
deactivate ":DefinirCategoriaController"


Administrativo <-- ":DefinirCategoriaUI": mostra a lista de competências técnicas

deactivate ":DefinirCategoriaUI"
loop
Administrativo -> ":DefinirCategoriaUI": seleciona competência técnica
activate ":DefinirCategoriaUI"
":DefinirCategoriaUI" -> ":DefinirCategoriaController": Obrigatoriedade()
activate ":DefinirCategoriaController"

":DefinirCategoriaController"-> ":Obrigatoria" : Obrigatoriedade()
activate ":Obrigatoria"
deactivate ":Obrigatoria"
deactivate ":DefinirCategoriaController"
":DefinirCategoriaUI" --> Administrativo: solicita obrigatoriedade(S/N)
Administrativo -> ":DefinirCategoriaUI": introduz informacão pedida
end
":DefinirCategoriaUI" -> ":DefinirCategoriaController": novaCategoria(desc)
activate ":DefinirCategoriaController"
":DefinirCategoriaController"-> ":Plataforma": cat=novaCategoria(desc)
activate ":Plataforma"
":Plataforma"-->"cat:Categoria" **: create(desc)

":DefinirCategoriaController"->":Plataforma": validaCategoria(cat)
deactivate ":Plataforma"

":DefinirCategoriaUI" --> Administrativo: apresenta dados e solicita confirmação

deactivate ":DefinirCategoriaUI"
Administrativo -> ":DefinirCategoriaUI": confirma
activate ":DefinirCategoriaUI"
":DefinirCategoriaUI"->":DefinirCategoriaController": registaCategoria()

":DefinirCategoriaController"->":Plataforma": registaCategoria(cat)
activate ":Plataforma"
":Plataforma" -> ":Plataforma" : validaCategoria(cat)



":DefinirCategoriaUI" --> Administrativo: operação bem sucedida

deactivate ":Plataforma"
deactivate ":DefinirCategoriaUI"
deactivate Administrativo
@enduml