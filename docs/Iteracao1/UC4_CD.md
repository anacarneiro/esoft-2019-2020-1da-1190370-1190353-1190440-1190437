@startuml

class CompetenciaTecnica {
  -String codigo
  -String descBreve
  -String descDetalhada
  -AreaAtividade area

  +CompetenciaTecnica(String codigo, String descBreve, String descDetalhada)
}

class Plataforma {
  -String designacao

  +getAreaAtividade()
  +getAreaAtividadePeloCodigo(String codigoArea)
  +CompetenciaTecnica novaCompetenciaTecnica(String codigo, String descbreve, String descDetalhada, AreaAtividade area)
  AreaAtividade novaAreaAtividade(String cod, String dsBreve, String dsDet)
  validaCompetenciaTecnica(CompetenciaTecnica compTec)
  registaCompetenciaTecnica(CompetenciaTecnica compTec)
  - addCompetenciaTecnica(CompetenciaTecnica compTec)
}

class EspecificarCompetenciaTecnicaController {
  +novaCompetenciaTecnica(String codigo, String descBreve, String descDetalhada)
  +registaCompetenciaTecnica()
}

class EspecificarCompetenciaTecnicaUI {
}

EspecificarCompetenciaTecnicaUI ..> EspecificarCompetenciaTecnicaController
EspecificarCompetenciaTecnicaController ..> Plataforma
EspecificarCompetenciaTecnicaController ..> CompetenciaTecnica
Plataforma "1" --> "*" CompetenciaTecnica : possui


@enduml
