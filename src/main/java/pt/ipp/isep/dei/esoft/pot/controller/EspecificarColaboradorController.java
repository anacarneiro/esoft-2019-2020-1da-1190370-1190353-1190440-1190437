package pt.ipp.isep.dei.esoft.pot.controller;


import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author Bruna Coutinho
 * @since 02/04/2020
 */
public class EspecificarColaboradorController {
    
    private AplicacaoPOT m_oApp;
    private Plataforma m_oPlataforma;
    private SessaoUtilizador m_Sessao;
    private Organizacao m_oOrganizacao;
    private Colaborador m_oColaborador;
    private String m_email;
    public EspecificarColaboradorController()
    {
        this.m_oApp = AplicacaoPOT.getInstance();
        this.m_oPlataforma = m_oApp.getPlataforma();
        this.m_Sessao = m_oApp.getSessaoAtual();
        this.m_email = m_Sessao.getEmailUtilizador();
        this.m_oOrganizacao = m_oPlataforma.getOrganizacaoUtilizadorAtual(m_email);
    }
    
    
    public boolean novoColaborador(String strNome, String strFuncao, String strTelefone, String strEmail)
    {
        try
        {  
            this.m_oColaborador = this.m_oOrganizacao.novoColaborador(strNome,strFuncao,strTelefone,strEmail);
            return this.m_oOrganizacao.validaColaborador(this.m_oColaborador);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oColaborador = null;
            return false;
        }
    }
    
    public boolean registaColaborador()
    {
        return this.m_oOrganizacao.registaColaborador(this.m_oColaborador);
    }

    public String getColaboradorString()
    {
        return this.m_oColaborador.toString();
    }
}
