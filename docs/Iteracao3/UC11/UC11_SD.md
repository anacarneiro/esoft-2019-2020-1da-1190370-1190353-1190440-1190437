@startuml
autonumber

actor "Freelancer" as FRL
participant ":AtualizarCandidaturaUI" as UI
participant ":AtualizarCandidaturaController" as CTRL
participant ":Plataforma" as PLAT
participant "ranu:\nRegistoAnuncios" as RANU
participant "rfree:\nRegistoFreelancer" as RFREEL
participant "listCan:\n List<Candidaturas>" as LSTCAN
participant "anu:\nAnuncio" as ANUN
participant "freel:\nFreelancer" as FREEL
participant "can:\nCandidatura" as CAND


activate FRL
FRL -> UI : inicia atualização de candidatura
activate UI

UI -> CTRL : listCan = getCandidaturas()
activate CTRL

CTRL -> PLAT: rfree = getRegistoFreelancers()
activate PLAT
deactivate PLAT

CTRL -> RFREEL : freel=getFreelancerByEmail(email)
note right
O email foi obtido através do objeto
SessaoUtilizador como demonstrado noutros diagramas.
end note
activate RFREEL
deactivate RFREEL
CTRL -> PLAT:  ranu = getRegistoAnuncios()
activate PLAT
deactivate PLAT

CTRL -> RANU:  listCan = getCandidaturas(freel)

activate RANU
RANU -> RANU : verificarDataLimite(dtFimCandidatura, dataAtual)
deactivate CTRL
UI --> FRL : apresenta lista de candidaturas

deactivate RANU

deactivate UI
FRL -> UI : selecionar candidatura a atualizar
activate UI
UI -> CTRL : getCanByID(anuID, canID)
activate CTRL
CTRL -> RANU : anu = getAnuncioByID(anuID)
activate RANU
deactivate RANU
CTRL -> ANUN : can = getCanByID(canID)
activate ANUN
deactivate ANUN
UI --> FRL : apresenta dados para atualizar na candidatura \n(valorPrt,nrDias,txtApresentacao,txtMotivacao)
deactivate CTRL
deactivate UI

FRL -> UI : introduz os dados que deseja atualizar \n(valorPrt,nrDias,txtApresentacao,txtMotivacao)
activate UI
UI -> CTRL : setCandidatura(valorPrt, nrDias, txtApres, txtMotiv)
activate CTRL




CTRL-> CAND: setCandidatura(valorPrt, nrDias, txtApres, txtMotiv)
activate CAND
deactivate CAND
deactivate CTRL
UI --> FRL : apresenta dados da candidatura e solicita confirmacao
deactivate UI

FRL -> UI : confirma dados da candidatura
activate UI
UI -> CTRL : AtualizaCand()
activate CTRL
CTRL -> RANU: AtualizaCand(anu, can)
activate RANU
RANU -> RANU : verificarDataLimite(dtFimCandidatura, dataAtual)
RANU -> RANU : registaCandidaturaAtualizada()
deactivate RANU
deactivate CTRL
UI -> FRL: operação concluída com sucesso
deactivate FRL
deactivate UI

deactivate CTRL
@enduml