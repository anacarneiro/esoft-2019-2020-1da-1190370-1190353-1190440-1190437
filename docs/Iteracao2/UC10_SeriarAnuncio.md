# UC10-Seriar Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a seriação de um anúncio.
O sistema solicita os dados necessários (i.e. data, hora) e outros colaboradores que intervenham no processo.
O colaborador de organização introduz os dados solicitados. O sistema **valida** e apresenta os dados ao colaborador de organização, pedindo que os confirme.
O colaborador de organização confirma. O sistema **regista os dados** e informa o colaborador de organização do sucesso da operação.


### SSD
![UC10_SSD.svg](UC10_SSD.svg)


### Formato Completo

#### Ator principal

* Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende seriar um anúncio.
* **Organização:** pretende que os seus colaboradores possam seriar um anúncio para posterior escolha de um freelancer.
* **Freelancer:** pretende ser escolhido para a tarefa á qual se candidatou.
* **T4J:** pretende que seja atribuida uma tarefa de um anúncio a um freelancer.


#### Pré-condições
* Tem que existir anúncios no sistema.

#### Pós-condições
* A seriação/ordenação de um anúncio é registado no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a seriação de uma anúncio.
2. O sistema mostra a lista de anúncios publicados pelo colaborador.
3. O colaborador seleciona o anúncio que pretende seriar.
4. O sistema mostra a lista de candidaturas ao anúncio escolhido.
5. O colaborador de organização escolhe uma candidatura atribuindo-lhe uma classificação.
6. O sistema apresenta uma lista de outros colaboradores para que o colaborador escolha quem participa no processo.
7. O colaborador escolhe outros colaboradores que participem no processo.
8. O sistema solicita ao colaborador de organização os dados necessários (i.e data e hora).
9. O colaborador introduz os dados necessários.
10. O sistema apresenta os dados e solicita confirmação.
11. O colaborador de organização confirma os dados.
12. O sistema informa do sucesso da operação.


#### Extensões ou fluxos alternativos

*a. O colaborador de organização solicita o cancelamento da seriação do anúncio.

> O caso de uso termina.

5a. A tarefa pretendida ainda não foi publicada e portanto o anúncio não existe.
>	1. O colaborador informa o sistema de tal facto. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador de organização não altera os dados. O caso de uso termina.

6c. O sistema deteta que não existem freelancers adequados ao anúncio pretendido.
>1.O sistema alerta o colaborador de organização para o facto. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Quando não existem freelancers adequados ao anúncio é necessário informar alguém sobre tal? O anúncio deverá continuar publicado?
* A lista de anúncios pode ser extensa faz sentido existir algum tipo de filtro?
* Há um número máximo de colaboradores que podem fazer parte da seriação de um anúncio?
* Qual a frequência de ocorrência deste caso de uso?
* Um anúncio não pode ser seriado por apenas um colaborador?
* Não podem a data e a hora serem geradas automaticamente pelo sistema?
* Depois de ser seriado o anúncio continua publicado no sistema?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.svg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional
| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a seriação de uma anúncio.  |	... interage com o utilizador? | SeriarAnuncioUI |  Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|  		 |	... coordena o UC?	| SeriarAnuncioController | Controller    |
|  		 |	... cria instância de Seriacao? | RegistoSeriacao | HC+LC   |
||...conhece o utilizador/gestor a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
||...sabe a que organização o utilizador/colaborador pertence?|RegistoColaborador|HC+LC.|
|||Colaborador|IE: conhece os seus dados (e.g. email). |
| 2. O sistema mostra a lista de anúncios publicados pelo colaborador.  		 |	...conhece a lista de anuncios?						 |    RegistoAnuncios         |      HC+LC                        |
| 3. O colaborador seleciona o anúncio que pretende seriar.  		 |	  |    |      |
| 4. O sistema mostra a lista de candidaturas ao anúncio escolhido. 		 |	... conhece a lista de candidaturas?  |  RegistarCandidatura  |  HC+LC   |
| 5. O colaborador de organização escolhe uma candidatura atribuindo-lhe uma classificação. | ... guarda a candidatura selecionada?	| ListaCandidaturaClassificada |  HC+LC          |
| 6. O sistema apresenta uma lista de outros colaboradores para que o colaborador escolha quem participa no processo.  		 |	... conhece a lista de colaboradores?| RegistoColaborador | HC+LC|
| 7. O colaborador escolhe outros colaboradores que participem no processo. 		 | ...guarda os colaboradores selecionados?							 | ListaParticipantes            |     HC+LC                         |
| 8. O sistema solicita ao colaborador de organização os dados necessários (i.e data e hora).	 |	 |   |  |
| 9. O colaborador introduz os dados necessários. | ...guarda os dados introduzidos? | RegistoSeriacao | HC+LC |
| 10. O sistema apresenta os dados e solicita confirmação. | ... valida os dados da seriação(validação local)? | Seriacao | I.E possui os seus próprios dados|
|      |...valida os dados da seriação(validação global)? | RegistoSeriacao | HC+LC|
|11. O colaborador de organização confirma os dados. |   |    |    |
|12. O sistema informa do sucesso da operação. | ...guarda a seriação criada? | ListaSeriacao | HC+LC|

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Seriacao
 * Classificacao
 * colaborador

Outras classes de software (i.e. Pure Fabrication) identificadas:

 * SeriarAnuncioUI
 * SeriarAnuncioController
 * RegistoAnuncio
 * RegistoSeriacao
 * RegistoColaborador
 * RegistoCandidatura
 * Classificacao
 * ListaCandidaturaClassificada
 * ListaParticipantes
 * ListaSeriacao

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

 ###	Diagrama de Sequência

 ![UC10_SD.svg](UC10_SD.svg)

 ###	Diagrama de Classes

 ![UC10_CD.svg](UC10_CD.svg)
