@startuml


autonumber

actor "Administrativo" as ADM
participant ":Registar\nFreelancerUI" as UI
participant ":RegistarFreelancer\nController" as CTRL
participant ":Plataforma" as PLAT
participant "fre:Freelancer" as _FRE
participant "Freelancer" as FRE
participant "end:EnderecoPostal" as END
participant "for: \nFormacao" as FOR
participant "ct: \nCompetenciaTecnica" as CT
participant "gr: \nGraudeProficiencia" as GR
participant ":AutorizacaoFacade" as AUT



activate ADM
ADM -> UI : registar freelancer
activate UI
UI --> ADM : solicita dados Freelancer\n(nome, NIF, endereco postal, contacto telefónico, email)
deactivate UI
ADM -> UI : introduz os dados solicitados
activate UI

UI -> CTRL : novoFreelancer(nome, NIF, endereco postal, contacto telefónico, email)
activate CTRL

CTRL -> PLAT: fre = novoFreelancer(nome, NIF, endereco postal, contacto telefónico, email)
activate PLAT

PLAT -> FRE: end=novoEndereco(endLocal,endCodPostal,endLocalidade)
activate FRE
PLAT -> _FRE**: create(nome, NIF, end, contacto telefónico, email)
FRE --> END ** : create(endLocal,endCodPostal,endLocalidade)
deactivate FRE
deactivate PLAT
CTRL -> PLAT : validaFreelancer(fre)
activate PLAT
PLAT -> PLAT: validaFreelancer(fre)
PLAT -> PLAT: addFreelancer(fre)

deactivate PLAT
loop
UI -> ADM : solicita formacao(instituicao, curso, grau, media de curso)
ADM -> UI : introduz dados
UI -> CTRL : addFormacao(freelancer, instituicao, curso, grau, media de curso)
CTRL -> FRE : addFormacao(fre, instituicao, curso, grau, media de curso)
activate FRE



FRE -> FOR** : create(instituicao, curso, grau, media de curso)

FRE -> FRE : validaFormacao(for)
FRE -> FRE : addFormacao(for)
deactivate FRE


end
loop
UI -> ADM : solicita competencias tecnicas(competencia tecnica, grau de proficiencia)
ADM -> UI : introduz dados
UI -> CTRL : addCT(freelancer, competencia tecnica, grau de proficiencia)
CTRL -> FRE : addCT(fre, competencia tecnica, grau de proficiencia)
activate FRE



FRE -> CT** : create(competencia tecnica)
FRE -> GR** : create(grau de proficiencia)
FRE -> FRE : validaCT(ct, gr)
FRE -> FRE : addCT(ct, gr)
deactivate FRE


end
deactivate CTRL
UI --> ADM : apresenta dados e solicita confirmação

deactivate UI

ADM -> UI : confirma
activate UI
UI -> CTRL: registaFreelancer()
activate CTRL
CTRL -> PLAT: registaFreelancer(fre, pwd)
activate PLAT
PLAT -> PLAT: validaFreelancer(fre, pwd)
PLAT -> _FRE: nome = getNome()
activate _FRE
deactivate _FRE
PLAT -> _FRE : email = getEmail()
activate _FRE
deactivate _FRE
PLAT -> AUT: registaUtilizadorComPapeis(nome, email,pwd,["FREELANCER"])
activate AUT
deactivate AUT
PLAT -> PLAT: addFreelancer(fre)

deactivate PLAT


deactivate CTRL
UI --> ADM : operação bem sucedida
deactivate UI

@enduml