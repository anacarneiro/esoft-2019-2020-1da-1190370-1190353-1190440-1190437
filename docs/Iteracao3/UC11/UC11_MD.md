@startuml
skinparam classAttributeIconSize 0
hide methods
left to right direction

class Plataforma {
  -String designacao
}


class Tarefa {
  -String referencia
  -String designacao
  -String descInformal
  -String descTecnica
  -Integer duracaoEst
  -Double custoEst
}

class Freelancer {
    -String nome
    -String NIF
    -String telefone
    -String email
}


class Anuncio
{
    -Date dtInicioPublicitacao
    -Date dtFimPublicitacao
    -Date dtInicioCandidatura
    -Date dtFimCandidatura
    -Date dtInicioSeriacao
    -Date dtFimSeriacao
}

class Candidatura
{
    -Date dataCandidatura
    -Double valorPretendido
    -Integer nrDias
    -String txtApresentacao
    -String txtMotivacao
}



Plataforma "1" -- "*" Anuncio : publicita >
Plataforma "1" -- "*" Freelancer : tem/usa  >




Anuncio "0..1" -- "1" Tarefa: publicita >
Anuncio "0..1" -- "1" Tarefa: da origem <
Anuncio "1" -- "*" Candidatura: recebe >



Candidatura "*" -- "1"  Freelancer: realizada por >
Candidatura "*" -- "1"  Freelancer: atualizada por >
@enduml