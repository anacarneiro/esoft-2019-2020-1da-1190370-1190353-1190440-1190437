@startuml
autonumber
hide footbox
actor "Gestor de Organização" as GO

participant ":AdjudicarAnuncioUI" as UI
participant ":AdjudicarAnuncioController" as CTRL
participant ":Aplicacao" as AP
participant "app\n:Aplicacao" as APP
participant "sessao\n:SessaoUtilizador" as SU
participant ":Plataforma" as PLAT
participant "rorg\n:RegistoOrganizacao" as RO
participant "rorg" as RORG
participant ":Lista<Organizacao>" as LO
participant ":org\n:Organizacao" as ORG
participant ":Lista<Colaborador>" as LC
participant ":colab\n:Colaborador" as COLAB
participant ":RegistoPublicarTarefa" as REGPT
participant "rpt\n:RegistoPublicarTarefa" as RPT
participant "TarefaPub\n:TarefaPublicada" as TP
participant "listaApp\n:ListaAplicacao" as LA
participant "listaApps\n:Lista<Aplicacao>" as LAS
participant "primeiraApp\n:Aplicacao" as PA
participant "freelancer\n:Freelancer" as FL
participant "processoAdjudi\n: ProcessoAjudicacao" as PADJ

activate GO
GO -> UI : inicia a adjudicação de um novo anúncio
activate UI

activate UI
UI -> CTRL : listaPT_getPubTarefaAdjudicacaoOpcional()

activate CTRL
CTRL -> AP : app=getInstancia()
activate AP
deactivate AP

CTRL -> APP : sessao=getSessaoAtual()
activate APP
deactivate APP

CTRL -> SU : email=getEmailUtilizador()
activate SU
deactivate Su

CTRL -> PLAT : rorg=getRegistoOrganizacao()
activate PLAT
deactivate PLAT

CTRL -> RORG : org=getOrganizacaoPorEmailUtilizador()
activate RO
activate RORG
RORG -> RORG : orgReturn=null

loop

RORG -> LO : org=get(i)
activate LO
deactivate LO

RORG -> ORG : temColaboradorComEmail(email)

activate ORG

ORG -> ORG : encontrado=falso

  loop

  ORG -> LC : colab=get(i)
  activate LC
  deactivate LC

  ORG -> COLAB : encontrado=temEmail(email)
  activate COLAB
  deactivate COLAB

  loop end

  ORG -> RORG : encontrado
  deactivate ORG

  RORG -> RORG : orgReturn=org

  RORG -> RO : orgReturn

loop end

deactivate RORG
deactivate RO

CTRL -> PLAT : rpt=getRegistoPublicarTarefa()
activate PLAT
deactivate PLAT

CTRL -> RPT : listaPT=listaPT_getPubTarefaAdjudicacaoOpcional(org)
activate RPT
deactivate RPT
deactivate CTRL

UI -> GO : apresenta a lista de todas as tarefas publicadas pela organização e pede que seja selecionado uma
deactivate UI

GO -> UI : seleciona uma tarefa publicada

activate UI

UI -> CTRL : primeiraApp=getPrimeiraAplicacao(IdPuTarefa)

activate CTRL

CTRL -> RPT : PubTarefa=getTarefaPubPorId(IdPubTarefa)
activate RPT
deactivate RPT

CTRL -> TP :listaApp=getListaAplicacao()
activate TP
deactivate TP

CTRL -> LA : listaApps=getAplicacoes()
activate LA
deactivate LA

CTRL -> LAS : primeiraApp=getPrimeiraAplicacao()
activate LAS
deactivate LAS

deactivate CTRL

UI -> GO : apresenta os dados da melhor candidatura e solicita confirmação ao Gestor de Organização em relação à escolha daquele freelancer
deactivate UI

GO -> UI : confirma
activate UI

UI -> PA : freelancer =getFreelancer()
activate PA
deactivate PA

UI -> GO : solicita os dados necessários para adjudicar a tarefa publicada (i.e. período de realização da tarefa e remuneração)
deactivate UI

GO -> UI : introduz os dados solicitados

activate UI

UI -> CTRL : novoProcessoAjudicacao(org, freelancer, tarefaPub, iniciarTarefa, acabarTarefa, remun)

activate CTRL

CTRL -> TP : tarefa=getTarefa()
activate TP
deactivate TP

CTRL -> RPT : numAdjudi=getQtdDeProcessosDeAjudi(DataAtual.agora().getAno())
activate RPT
deactivate RPT

CTRL -> TP : ProcessoAdjudi=novoProcessoAjudicacao(org,freelancer,TarefaPub,tarefa,iniciarTarefa,acabarTarefa,valorRemun)
activate TP

TP -> TP : adjuSeqNum=generarSeqNum(numAdjudi)

TP -> TP : tempoAdjudi=DataAtual.agora()

TP -> PADJ : criar(seqAdjudi,org,freelancer,tarefaPub,tarefa,iniciarTarefa,acabarTarefa,valorRemun,tempoAdjudi)
activate PADJ
deactivate PADJ

TP -> TP : validarProcessoAdjudi(processoAdjudi)

deactivate TP

UI -> GO : apresenta os dados e solicita confirmação
deactivate UI

GO -> UI : confirma
activate UI

UI -> CTRL : registoProcessoAdjudi

activate CTRL

CTRL -> TP : resgistoProcessoAdjudi(processoAdjudi)

activate TP

TP -> TP : validarProcessoAdjudi(processoAdjudi)

TP -> FL : atribuir(processoAdjudi)
activate FL
deactivate FL

TP -> TP : setProcessoAdjudi(ProcessoAdjudi)

deactivate TP
deactivate CTRL

UI -> GO : atribui a tarefa e informa sobre o sucesso da operação
deactivate UI

deactivate GO

@enduml
